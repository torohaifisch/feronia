extends Node

var p_scene=preload("res://modo_pelea/escenas/player/Player.tscn")
var b_scene=preload("res://modo_pelea/escenas/niveles/lvl1.tscn")
var player
var side_player
var beat_scene = null
var sidescroll_scene=null
var current_enemy=null
var maps = ["res://modo_normal/escenas/niveles/level_tutorial.tscn",
			"res://modo_normal/escenas/niveles/nivel_1.tscn",
			"res://global/gui/escenas/menu principal/menu_principal.tscn",
			"res://modo_normal/escenas/niveles/nivel_1_2.tscn",
			"res://modo_normal/escenas/niveles/nivel_2.tscn",
			"res://modo_normal/escenas/niveles/nivel_2_1.tscn",
			"res://modo_normal/escenas/niveles/nivel_1_3.tscn",
			"res://modo_normal/escenas/niveles/nivel_3.tscn",
			"res://modo_normal/escenas/niveles/nivel_4.tscn",
			"res://global/gui/escenas/intro/introduction.tscn",
			"res://global/gui/escenas/fin_isma/postscene_ismaboss.tscn"]

#niveles donde se puede teletransportar (posicion del array de arriba)
var altares = [1,4,7]
var current_place = 'start'


#var save_loaded={state=false,
#			save_file='res://savefiles/save.json'}


func set_sideplayer():
	set_sidescroll_scene()
	side_player=sidescroll_scene.get_node('Player')

func spawn_player(pos):
	get_scene()
	player=p_scene.instance()
	beat_scene.add_child(player)
	player.set_pos(pos)
	if side_player!=null:
		player.hp=side_player.hp
		signal_manager.update_max_hp(side_player.max_hp)
		signal_manager.update_hp(side_player.hp)
		
	
# esto se puede optimizar al guardar la prosicion previa y la actual asi solo hacer sort a los nodos que cambiaron su posicion
func sort_z():
	var nodes=get_tree().get_nodes_in_group('sortable')
	for i in nodes:
		if i.z_index!=i.shadow.global_position.y:
			i.z_index=i.shadow.global_position.y

func battle_mode():

	call_deferred('_deferred_battle_mode')
		
func _deferred_battle_mode():
		var bg=sidescroll_scene.bg
		get_tree().get_root().remove_child(sidescroll_scene)
		var battle=b_scene.instance()
		battle.set_bg(bg)
		get_tree().get_root().add_child(battle)
		get_tree().paused = false
		# se debiese iterar por todo el arreglo de current enemy
		# en este caso se saca solo el primer enemigo y se pasan sus valores
		#for ene in current_enemy.arr:
		var ene=current_enemy.vec
		battle.spawn_enemies(ene.x,ene.y,ene.z)

func start_mode(scene_root,nivel_numero,port=false):

	call_deferred('_deferred_switch_scene',scene_root,nivel_numero,port)

#se utiliza al cambiar entre escenas dentro del juego
func _deferred_switch_scene(c,n,p):
	
	var dict=null
	#si no estamos en el menu principal entonces se obtiene el nodo jugador y se guarda el estado
	if c.name!='menu_principal':
		dict=c.get_node('Player').save_state()
	
	#se elimina la escena actual
	get_tree().get_root().remove_child(c)
	c.queue_free()
	if beat_scene!=null:
		beat_scene.queue_free()
	
	#se carga la escena siguiente
	var lvl =load(maps[n])
	var start=lvl.instance()
	
	get_tree().get_root().add_child(start)
	
	#si el diccionario tiene datos entonces se obtiene el jugador de la nueva escena y se actualiza su estado
	if dict!=null and n!=2:
		start.get_node('Player').load_state(dict)
		signal_manager.update_hp(side_player.hp)
		signal_manager.emit_signal("money_change")
		signal_manager.pociones_change()
		signal_manager.update_lvl()
	
	# si teleportear es verdadero se obtiene el altar existente en el nivel y se setea la posicion del player a la del altar
	# nunca se buscara un altar en un nodo que no lo tenga ya que el nivel a teleportear se especifica antes al llamar la func de teleport
	if p==true:
		var node=start.get_node('altar')
		
		start.get_node('Player').global_position=node.global_position

func deferred_no_save(c,n):
	call_deferred('no_save',c,n)
func no_save(c,n):
	
	#se elimina la escena actual
	get_tree().get_root().remove_child(c)
	c.queue_free()
	if beat_scene!=null:
		beat_scene.queue_free()
	
	#se carga la escena siguiente
	var lvl =load(maps[n])
	var start=lvl.instance()
	side_player=start.get_node('Player')
	get_tree().get_root().add_child(start)
	



#para volver desde el modo beat em up a sidescroll
func back_to_sidescroll(node):
	#get_tree().get_root().remove_child(node)
	#instancia de batalla se libera de memoria
	
	node.queue_free()
	beat_scene=null
	#se vuelve a sidescroll añadiendo el nodo a la rama
	get_tree().get_root().add_child(sidescroll_scene)
	#señal para hacer transicion
	signal_manager.update_max_hp(side_player.max_hp)
	signal_manager.update_hp(side_player.hp)
	
	signal_manager.transicion_a_sidescroll()
	signal_manager.emit_signal("money_change")
	signal_manager.update_lvl()
	if current_enemy.name=='isma_side':
		start_mode(sidescroll_scene, 10, false)
	#se libera al enemigo derrotado en el modo sidescroll
	current_enemy.queue_free()

#se utiliza al cargar la partida
func load_scene(scene_root,scene_path,dict=null):
	get_tree().get_root().remove_child(scene_root)
	scene_root.queue_free()
	if beat_scene!=null:
		
		beat_scene.queue_free()
		beat_scene=null
	var lvl =load(scene_path)
	var scene_instance=lvl.instance()
	get_tree().get_root().add_child(scene_instance)
	if dict!=null:
		scene_instance.get_node('Player').load_state(dict,true)
		signal_manager.update_hp(side_player.hp)
		signal_manager.emit_signal("money_change")
		signal_manager.update_lvl()
		signal_manager.pociones_change()

	
func teleport(scene_root,level):
	
	print(scene_root)
	start_mode(scene_root,altares[level],true)

	
func get_scene():
	var root = get_tree().get_root()
	beat_scene = root.get_child( root.get_child_count() -1 )
	
func set_sidescroll_scene():
	var root = get_tree().get_root()
	sidescroll_scene = root.get_child( root.get_child_count() -1 )
