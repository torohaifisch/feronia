extends Node

signal activate_player
signal deactivate_player
signal game_over_b
#GUI
signal activate_vendor
signal vendedor_pociones
signal activate_inventory
signal activate_options
signal activate_dialogue
signal enemy_info

signal equip_arma
signal equip_armadura
signal activate_tutorial

signal update_hp
signal update_max_hp
signal lvl_up
signal update_lvl

signal money_change
signal pociones_change

signal save_game
signal altar_gui

#TRANSICIONES
signal transicion_a_batalla
signal transicion_a_sidescroll
signal transicion_a_jefe
signal fade_in
signal fade_out


func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass
	
# emisor -> GUI en canvaslayer
# receptor -> player

func activate_player():
	emit_signal('activate_player')
	
# emisor -> GUI en canvaslayer
# receptor -> player
func deactivate_player():
	emit_signal('deactivate_player')


func activate_vendor(vendor):
	emit_signal('activate_vendor',vendor)
func activate_inventory():
	emit_signal('activate_inventory')

# emisor -> ?
# receptor -> canvaslayer de los niveles
func activate_options():
	emit_signal('activate_options')


#emisor-> world,confirmar
#receptor -> canvas layer
func money_change():
	emit_signal('money_change')

# emisor -> menu_gui/arma
# receptor -> menu gui
func equip_arma():
	print('si')
	emit_signal('equip_arma')
	
# emisor -> menu_gui/armadura
#receptor -> menu gui	
func equip_armadura():
	emit_signal('equip_armadura')

# emisor -> global
# receptor -> menu gui
func update_lvl():
	emit_signal('update_lvl')
	
func lvl_up():
	print('lvl up')
	emit_signal('lvl_up')
# emisor -> player / enemigos
# receptor -> canvaslayer
func transicion_a_batalla():
	emit_signal('transicion_a_batalla')
	
# emisor -> niveles beat em up 
# receptor -> canvaslayer
func transicion_a_sidescroll():
	emit_signal('transicion_a_sidescroll')
	
#emisor -> player
#receptor -> gui
func enemy_info(body):
	emit_signal('enemy_info',body)
	
func activate_tutorial(number):
	emit_signal('activate_tutorial',number)
# emisor -> player beat em up
# canvaslayer hp bar
func update_hp(n):
	emit_signal('update_hp',n)
# emisor-> player lvl up
# receptor-> canvaslayer hp bar
func update_max_hp(n):
	emit_signal('update_max_hp',n)

func activate_dialogue(file_path,node):
	emit_signal('activate_dialogue',file_path,node)

func game_over_b():
	emit_signal('game_over_b')
	
func save_game():
	emit_signal('save_game')

#altar -> altar gui
func altar_gui():
	emit_signal('altar_gui')
	
func pociones_change():
	emit_signal('pociones_change')