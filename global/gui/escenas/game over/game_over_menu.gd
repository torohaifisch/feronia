extends Container


var state= false
var last=1
var item_selected=0


func _ready():

	set_physics_process(true)
	visible=false
	$VBoxContainer/item/nombre.text='Continuar'
	$VBoxContainer/item2/nombre.text='Salir'
	
func _physics_process(delta):
	if state==true:
		check_input()

#checkea los inputs posibles
func check_input():
	if Input.is_action_just_pressed('ui_down'):
		if item_selected==last:
			item_selected=0
		else:
			item_selected+=1
		update_arrow()
		
	if Input.is_action_just_pressed('ui_up'):
		if item_selected==0:
			item_selected=last
		else:
			item_selected-=1
		update_arrow()
		
	
	if Input.is_action_just_pressed('attack'):
		if item_selected==0:
			save_load.load_game(global.sidescroll_scene)
		else:
			get_tree().quit()

#activa/desactivar flechas
func update_arrow():
	disable_arrow()
	enable_arrow()
# activa flechas
func enable_arrow():
	$VBoxContainer.get_child(item_selected).enable_arrow()

# desactiva flechas
func disable_arrow():
	for i in range(0,2):
		$VBoxContainer.get_child(i).disable_arrow()
# actualiza atributos del jugador


func open():
	if state==false:
		$Timer.start()
		update_arrow()
		

func _on_Timer_timeout():
	state=true
	visible=true