extends Panel



var state= false
var last=0
var total_items=0
var item_selected=0
var item_p= preload("res://global/gui/escenas/vendedor/vendor_list_item.tscn")

func _ready():

	set_physics_process(true)
	visible=false
	
func _physics_process(delta):
	if state==true:
		check_input()

#checkea los inputs posibles
func check_input():
	if Input.is_action_just_pressed('ui_down'):
		if item_selected==last:
			item_selected=0
		else:
			item_selected+=1
		update_arrow()
		
	if Input.is_action_just_pressed('ui_up'):
		if item_selected==0:
			item_selected=last
		else:
			item_selected-=1
		update_arrow()
		
		
	if Input.is_action_just_pressed('cancel'):
		close()
	
	if Input.is_action_just_pressed('attack'):
		global.teleport(get_parent().get_parent(),item_selected)


#activa/desactivar flechas
func update_arrow():
	disable_arrow()
	enable_arrow()
# activa flechas
func enable_arrow():
	$VBoxContainer.get_child(item_selected).enable_arrow()

# desactiva flechas
func disable_arrow():
	for i in range(0,3):
		$VBoxContainer.get_child(i).disable_arrow()
# actualiza atributos del jugador


func open():
	if state==false:
		$Timer.start()
		visible=true
		var counter=0
		for i in global.side_player.skills.values():
			if i==1:
				counter+=1
		total_items=counter
		last=counter-1
		print(last)
		load_places()
		update_arrow()
		
func load_places():
	for i in range(total_items):
		$VBoxContainer.get_child(i).visible=true
	
#se resetea todo a default al cerrar y se activa el jugador
func close():
	
	visible=false
	state=false

	item_selected=0
	signal_manager.activate_player()


	


func _on_Timer_timeout():
	state=true
