extends Panel

var status=false
var selected=0


func _ready():
	visible=false
	set_physics_process(true)
	
func _physics_process(delta):
	#si la ventana esta activa entonces se checkea el input y se actualiza la posicion de la flecha
	if status==true:
		disable_arrow()
		check_input()
		enable_arrow()
#checkea los posibles inputs
func check_input():
	if Input.is_action_just_pressed('ui_left'):
		
		if selected==0:
			selected =1
		else:
			selected=0
		
	if Input.is_action_just_pressed('ui_right'):
		
		if selected==1:
			
			selected=0
		else:
			selected=1
			
	if Input.is_action_just_pressed('cancel'):
		
		close()

	if Input.is_action_just_pressed('attack') :
		if selected==1:
			close()
		else:
			#añade el arma al inventario del jugador
			global.side_player.pociones+=1
			global.side_player.resta_monedas(50)
			#conectado a canvaslayer
			signal_manager.emit_signal('money_change')
			signal_manager.pociones_change()
			close()
			
#setea el alpha de color a 255
func enable_arrow():
	
	if selected==1:
		$HBoxContainer.get_child(selected+1).modulate=Color(255,255,255,255)
	else:
		$HBoxContainer.get_child(selected).modulate=Color(255,255,255,255)

#setea el alpha de color a 0 (transparente)
func disable_arrow():
	
	$HBoxContainer/arrow.modulate=Color(255,255,255,0)
	$HBoxContainer/arrow2.modulate=Color(255,255,255,0)

#abrir la ventana 
func open():
	visible=true
	disable_arrow()
	$Timer.start()

#cierra la ventana y resetea todo a default
func close():
	
	status=false
	selected=0
	get_parent().status=true
	visible=false
		
		

func _on_Timer_timeout():
	
	status=true
