extends Panel

#estado para saber si la ventana esta activa y poder interactuar con esta
var status= false
var item_p= preload("res://global/gui/escenas/vendedor/vendor_list_item.tscn")
#item actual seleccionado
var selected=0
#numero de items en total
var last=3
var vende


func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	set_physics_process(true)

func _physics_process(delta):

	if status==true:
		
		check_input()
		check_compra()
		check_close()
		
	
func open(vendedor):
	if status==false:
		visible=true
		vende=vendedor
		
		#carga los items desde el inventario
		var counter=0
		for i in vende.inventario:
			
			var item=$item_lista/VBoxContainer.get_child(counter)
			item.set_text(i.nombre)
			counter+=1
		$dialogo/VBoxContainer/dialogo.text='Compra o largate!!!'
		update_arrow()
		set_descripcion()
		
		$AnimationPlayer.play('open')
		
#checkea si es posible comprar, en este caso si el tamaño del inventario es mayor al item seleccionado(0-3)
#recordar que en un principio gorgo solo puede vender 4 items como maximo
func check_compra():
	
	if vende.inventario.size()>selected:
		if Input.is_action_just_pressed('attack'):
			#si el stock del item es mayor a 0
			if !global.side_player.has_item(vende.inventario[selected].nombre):
				#si tengo suficiente dinero
				if global.side_player.monedas>=vende.inventario[selected].precio:
					#se desactiva la ventana actual y se activa la ventana de confirmacion pasandole el nombre del item seleccionado
					status=false
					$confirmar.open(vende.inventario[selected])
				else:
					$dialogo/VBoxContainer/dialogo.text='No tienes dinero suficiente'
					$AnimationPlayer.play('dialogue_in')
			else:
				$dialogo/VBoxContainer/dialogo.text='Ya tienes este item'
				$AnimationPlayer.play('dialogue_in')
				
#checkea si se presiono el boton para cerrar y si fue asi resetea todos los estados
func check_close():
	if Input.is_action_just_pressed('cancel'):
		status=false
		selected=0
		$AnimationPlayer.play('close')


#checkea los inputs del jugador
func check_input():
	if Input.is_action_just_pressed('ui_down'):
		if selected==last:
			selected=0
		else:
			selected+=1
		update_arrow()
		set_descripcion()
		
	if Input.is_action_just_pressed('ui_up'):
		if selected==0:
			selected=last
		else:
			selected-=1
		update_arrow()
		set_descripcion()


func update_arrow():
	disable_arrow()
	enable_arrow()

#hace visible la flecha del item seleccionado actualmente
func enable_arrow():
	
		$item_lista/VBoxContainer.get_child(selected).enable_arrow()

#hace invisible todas las flechas
func disable_arrow():
	for i in range(0,4):
		$item_lista/VBoxContainer.get_child(i).disable_arrow()

#setea la descripcion del objeto si el tamaño del inventario es mayor al del item seleccionado
func set_descripcion():
	if vende.inventario.size()>selected:
		var item= vende.inventario[selected]
		
		$item_descripcion/VBoxContainer/descripcion.text=item.descripcion
		$item_descripcion/VBoxContainer/Precio.text='Precio: '+String(item.precio)
		$item_descripcion/VBoxContainer/dmg.text='Dmg: '+String(item.dano)
	else:
		#descripcion es vacia si no hay item
		$item_descripcion/VBoxContainer/descripcion.text=''

func _on_AnimationPlayer_animation_finished( anim_name ):
	if anim_name=='open':
		$AnimationPlayer.play('dialogue_in')
		status=true

	
	if anim_name=='close':
		visible=false
		signal_manager.activate_player()