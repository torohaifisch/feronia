extends Container

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var status=false
var selected=0

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	set_physics_process(true)
	$item_lista/VBoxContainer/item/nombre.text='Poción de Vida'

func _physics_process(delta):
	if status==true:
		check_input()
#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.

func open():
	if status==false:
		
		visible=true
		update_arrow()
		$dialogo/VBoxContainer/dialogo.text='¿Qué desea comprar?'
		$Timer.start()



func check_input():
	if Input.is_action_just_pressed('attack'):
		
		if global.side_player.monedas>=50:
			if global.side_player.pociones+1<=global.side_player.max_pociones:
				status=false
				$confirmar.open()
			else:
				$dialogo/VBoxContainer/dialogo.text='No puedes comprar más'
				$AnimationPlayer.play('dialogue_in')
		else:
			$dialogo/VBoxContainer/dialogo.text='No tienes dinero suficiente'
			$AnimationPlayer.play('dialogue_in')
			
	if Input.is_action_just_pressed('cancel'):
		print('potas')
		status=false
		visible=false
		signal_manager.activate_player()
		
		
		
func update_arrow():
	disable_arrow()
	enable_arrow()

#hace visible la flecha del item seleccionado actualmente
func enable_arrow():
	
		$item_lista/VBoxContainer.get_child(selected).enable_arrow()

#hace invisible todas las flechas
func disable_arrow():
	$item_lista/VBoxContainer.get_child(0).disable_arrow()

func _on_Timer_timeout():
	status=true
