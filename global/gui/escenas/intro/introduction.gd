extends Node2D

export(String, FILE) var file_path

func _ready():
	$CanvasModulate/AnimatedSprite/AnimationPlayer.play("sit")
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass

func _on_fade_animation_finished(fade_in):
	$CanvasModulate/diosa/AnimationGoddess.play('fade_in')

func _on_AnimationGoddess_animation_finished(fade_in):
	if $CanvasModulate/fade.current_animation != "fade_in":
		$CanvasModulate/diosa/AnimationGoddess.play("idle")
		$CanvasModulate/AnimatedSprite/AnimationPlayer.play("stand")


func _on_AnimationPlayer_animation_finished(stand):
	$CanvasModulate/AnimatedSprite/AnimationPlayer.play("idle")
	#Iniciar diálogo
	signal_manager.activate_dialogue(file_path,self)
	

func end_dialogue():
	$CanvasModulate/fade.play("fade_out")

func _on_fade_out_animation_finished(fade_out):
	if fade_out == "fade_out":
		global.start_mode(self, 0)

