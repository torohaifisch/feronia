extends Node2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var waiting=false
var active=false
func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	signal_manager.connect('activate_tutorial',self,'activate')
	set_physics_process(true)

func _physics_process(delta):
	if waiting==false and active:
		$Label.visible=true
		if Input.is_action_just_pressed('attack'):
			deactivate()
			
func activate(n):
	signal_manager.deactivate_player()
	$AnimatedSprite.frame=n
	$AnimatedSprite.visible=true

	$Timer.start()
	print('activo tim')
	waiting=true
	active=true
	
func deactivate():
	$AnimatedSprite.visible=false
	$Label.visible=false
	active=false
	signal_manager.activate_player()
	
func _on_Timer_timeout():

	waiting=false
