extends Node2D

export(String, FILE) var file_path
onready var timer = $Timer
# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

#func _process(delta):
#	pass



func _on_AnimationIsma_animation_finished(huir):
	$CanvasModulate/AnimatedGoddess/AnimationGoddess.play("appear")
	
func _on_AnimationGoddess_animation_finished(appear):
	$CanvasModulate/AnimatedGoddess/AnimationGoddess.play("idle_Goddess")

#iniciar dialogo 1 (diosa y feronia), al terminar iniciar dialogo 2 (isma y feronia)
	
func _on_Timer_timeout():
	signal_manager.activate_dialogue(file_path,self)

func end_dialogue():
	if $".".file_path == "res://modo_normal/dialogos/end_isma/end_isma_diosa.json":
		file_path = "res://modo_normal/dialogos/end_isma/end_isma.json"
		signal_manager.activate_dialogue(file_path,self)
	elif $".".file_path == "res://modo_normal/dialogos/end_isma/end_isma.json":
		pass
	$CanvasModulate/AnimatedGoddess.apply_scale(Vector2(-1,1))
	$CanvasModulate/fade.play("fade_out")
		



func _on_fade_animation_finished(fade_out):
	if fade_out == "fade_out":
		global.start_mode(self, 2)
