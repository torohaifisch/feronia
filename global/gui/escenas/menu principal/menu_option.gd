extends HBoxContainer

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

func disable_arrow():
	$arrow.modulate=Color(255,255,255,0)
func enable_arrow():
	$arrow.modulate=Color(255,255,255,255)