extends Panel

var status=false
var item_selected=0
var last=2

func _ready():

	set_physics_process(true)

func _physics_process(delta):
	if status==true:	
		check_input()


func check_input():
	if Input.is_action_just_pressed('ui_down'):
		if item_selected==last:
			item_selected=0
		else:
			item_selected+=1
		update_arrow()
		
	if Input.is_action_just_pressed('ui_up'):
		if item_selected==0:
			item_selected=last
		else:
			item_selected-=1
		update_arrow()

	if Input.is_action_just_pressed('cancel'):
		close()
	
	if Input.is_action_just_pressed('attack'):
		if item_selected==0:
			pass
		if item_selected==1:
			$video.open()
			status=false
		if item_selected==2:
			$controls.open()
			status=false

func update_arrow():
	disable_arrow()
	enable_arrow()

func enable_arrow():
	if item_selected<=last:
		$options.get_child(item_selected).enable_arrow()


func disable_arrow():
	for i in range(0,3):
		$options.get_child(i).disable_arrow()


func open():
	
	update_arrow()
	
	status=true
	$AnimationPlayer.play("open")
func close():
	
	status=false
	item_selected=0
	$AnimationPlayer.play('close')
	get_parent().status=true

func _on_AnimationPlayer_animation_finished( anim_name ):
	if anim_name=='open_video':
		$video.status=true
	if anim_name=='close_video':
		status=true
	if anim_name=='open_controls':
		$controls.status=true
	if anim_name=='close_controls':
		status=true