extends NinePatchRect

var item_selected=0
var last=4
var status=true

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	set_physics_process(true)
	update_arrow()
	
func _physics_process(delta):
	if status==true:
		check_input()
	
	if Input.is_action_just_pressed('save'):
		save_load.load_game(self)

func check_input():
	if Input.is_action_just_pressed('ui_down'):
		if item_selected==last:
			item_selected=0
		else:
			item_selected+=1
		update_arrow()
		
	if Input.is_action_just_pressed('ui_up'):
		if item_selected==0:
			item_selected=last
		else:
			item_selected-=1
		update_arrow()
	if Input.is_action_just_pressed('attack'):
		if item_selected==4:
			get_tree().quit()
		if item_selected==2:
			status=false
			$option_panel.open()
		if item_selected==0:
			save_load.delete_save()
			global.start_mode(self,9)
			
		if item_selected==1:
			save_load.load_game(self)
		if item_selected==3:
			status=false
			$credits.open()

func update_arrow():
	disable_arrow()
	enable_arrow()

func enable_arrow():
	
	if item_selected<=last:
		$VBoxContainer.get_child(item_selected).enable_arrow()


func disable_arrow():
	
	for i in range(0,5):
		$VBoxContainer.get_child(i).disable_arrow()

func _on_AnimationPlayer_animation_finished( anim_name ):
	if anim_name=='close_credits':
		status=true


