extends PanelContainer

var status= false
# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	set_physics_process(true)
	visible=false
	
func _physics_process(delta):
	if status==true:
		check_input()

func check_input():

	if Input.is_action_just_pressed('cancel'):
		close()

func open():
	get_parent().get_node('AnimationPlayer').play("open_controls")

func close():
	status=false
	get_parent().get_node('AnimationPlayer').play("close_controls")

func _on_AnimationPlayer_animation_finished( anim_name ):
	if anim_name=='close_controls':
		visible=false
	if anim_name=='open_controls':
		status=true