extends Panel

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var item_selected=0
var display_mode=0
var vsync=true
var status=false
var display=0
var last=2

func _ready():
	update_display()
	update_arrow()
	set_physics_process(true)

func _physics_process(delta):
	if status==true:
		check_input()

func check_input():
	
	if Input.is_action_just_pressed('attack'):
		if item_selected==0:
			if display_mode==0:
				display_mode=1
				
			else:
				display_mode=0
			update_display()
		if item_selected ==1:
			if vsync==true:
				vsync=false
			else:
				vsync=true
			update_display()
			
		if item_selected==2:
			
			apply()
			
	if Input.is_action_just_pressed('ui_up'):
		if item_selected!=0:
			item_selected-=1
			update_arrow()
	if Input.is_action_just_pressed('ui_down'):
		if item_selected!=2:
			item_selected+=1
			update_arrow()
	if Input.is_action_just_pressed('cancel'):
		close()
		
func apply():
	if display_mode==0:
		OS.window_fullscreen=false
	if display_mode==1:
		print('test')
		OS.window_fullscreen=true
	if vsync==true:
		OS.vsync_enabled=true
	else:
		OS.vsync_enabled=false
		
func update_display():
	if display_mode==0:
		$VBoxContainer/HBoxContainer/Label.text='Windowed'
	else:	
		$VBoxContainer/HBoxContainer/Label.text='FullScreen'
	
	if vsync==false:
		$VBoxContainer/HBoxContainer2/Label.text= 'Off'
	
	else:
		$VBoxContainer/HBoxContainer2/Label.text='On'

func update_arrow():
	disable_arrow()
	enable_arrow()

func enable_arrow():
	if item_selected<=last:
		$VBoxContainer.get_child(item_selected).enable_arrow()


func disable_arrow():
	for i in range(0,3):
		$VBoxContainer.get_child(i).disable_arrow()


func open():
	
	update_arrow()
	get_parent().get_node('AnimationPlayer').play('open_video')
	
func close():
	
	status=false
	item_selected=0
	get_parent().get_node('AnimationPlayer').play('close_video')
