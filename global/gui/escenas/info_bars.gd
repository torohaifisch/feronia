extends Container

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	signal_manager.connect('update_hp',self,'_on_hp_update')
	signal_manager.connect('update_max_hp',self,'_on_max_hp_update')

func _on_hp_update(n):
	$hp_bar.value=n
	
func _on_max_hp_update(n):
	$hp_bar.max_value=n