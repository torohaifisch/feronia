extends HBoxContainer

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
func set_text(n):
	$name.text=n

func disable_arrow():
	$arrow.modulate=Color(255,255,255,0)
func enable_arrow():
	$arrow.modulate=Color(255,255,255,255)

func enable_equiped():
	$equipped.modulate=Color(255/255,255/255,255/255,1)
func disable_equiped():
	$equipped.modulate=Color(255/255,255/255,255/255,0)