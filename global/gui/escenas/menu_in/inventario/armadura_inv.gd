extends Panel

var status= false
var page_selected=0
var last=3
var item_selected=0

func _ready():

	set_physics_process(true)
	visible=false

func _physics_process(delta):
	if status==true:
		check_input()


func check_input():
	if Input.is_action_just_pressed('ui_down'):
		if item_selected==last:
			item_selected=0
		else:
			item_selected+=1
		update_arrow()
		
	if Input.is_action_just_pressed('ui_up'):
		if item_selected==0:
			item_selected=last
		else:
			item_selected-=1
		update_arrow()
		
	if Input.is_action_just_pressed('ui_left'):
		if page_selected!=0:
			page_selected-=1
			load_items()
			update_page_number()
			update_equiped()
			
	if Input.is_action_just_pressed('ui_right'):
		if page_selected<ceil(global.side_player.inv_armadura.size()/4.0-1):
			page_selected+=1
			load_items()
			update_page_number()
			update_equiped()
		
	if Input.is_action_just_pressed('cancel'):
		close()
	
	if Input.is_action_just_pressed('attack'):
		equip()
		update_equiped()
	
func update_arrow():
	disable_arrow()
	enable_arrow()

func enable_arrow():
	$lista_items/VBoxContainer.get_child(item_selected).enable_arrow()


func disable_arrow():
	for i in range(0,4):
		$lista_items/VBoxContainer.get_child(i).disable_arrow()

func load_items():
	for i in range(4*page_selected,4*page_selected+4):
		if i < global.side_player.inv_armadura.size():
			$lista_items/VBoxContainer.get_child(i%4).set_text(global.side_player.inv_armadura[i].nombre)
		else:
			$lista_items/VBoxContainer.get_child(i%4).set_text('')

func update_equiped():
	for i in range(0,4):
		$lista_items/VBoxContainer.get_child(i).disable_equiped()
		
		if global.side_player.arma!=null:
			if $lista_items/VBoxContainer.get_child(i).get_node('name').text==global.side_player.arma.nombre:
						$lista_items/VBoxContainer.get_child(i).enable_equiped()

func equip():
	if !global.side_player.inv_armadura.empty():	
		if page_selected==ceil(global.side_player.inv_armadura.size()/4.0)-1:
		
			if item_selected<=(global.side_player.inv_armadura.size()-1)%4:
				global.side_player.arma=global.side_player.inv_armadura[item_selected+(page_selected*4)]
				signal_manager.emit_signal('equip_armadura')
		
		else:
			global.side_player.arma=global.side_player.inv_armadura[item_selected+(page_selected*4)]
			signal_manager.emit_signal('equip_armadura')
		
func open():
	load_items()
	update_arrow()
	update_page_number()
	update_equiped()
	visible=true
	status=true
	
func close():
	visible=false
	status=false
	item_selected=0
	page_selected=0
	get_parent().open_main()

func update_page_number():
	$lista_items/VBoxContainer/numero_pagina.text='< '+String(page_selected)+' >'