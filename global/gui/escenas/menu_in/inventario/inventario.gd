extends Panel

var status= false
var page_selected=0
var last=3
var item_selected=0

func _ready():

	set_physics_process(true)
	visible=false

func _physics_process(delta):
	if status==true:
		check_input()


func check_input():
	if Input.is_action_just_pressed('ui_down'):
		if item_selected==last:
			item_selected=0
		else:
			item_selected+=1
		update_arrow()
		
	if Input.is_action_just_pressed('ui_up'):
		if item_selected==0:
			item_selected=last
		else:
			item_selected-=1
		update_arrow()
		
	if Input.is_action_just_pressed('ui_left'):
		if page_selected!=0:
			page_selected-=1
			load_items()
			update_page_number()
			update_equiped()
			
	if Input.is_action_just_pressed('ui_right'):
		if page_selected<ceil(global.side_player.inv_arma.size()/4.0-1):
			page_selected+=1
			load_items()
			update_page_number()
			update_equiped()
		
	if Input.is_action_just_pressed('cancel'):
		close()
	
	if Input.is_action_just_pressed('attack'):
		equip()
		update_equiped()
	
func update_arrow():
	disable_arrow()
	enable_arrow()

func enable_arrow():
	$lista_items/VBoxContainer.get_child(item_selected).enable_arrow()


func disable_arrow():
	for i in range(0,4):
		$lista_items/VBoxContainer.get_child(i).disable_arrow()

#carga los items del inventario del jugador
func load_items():
	for i in range(4*page_selected,4*page_selected+4):
		if i < global.side_player.inv_arma.size():
			$lista_items/VBoxContainer.get_child(i%4).set_text(global.side_player.inv_arma[i].nombre)
		else:
			$lista_items/VBoxContainer.get_child(i%4).set_text('')

#activa la letra E para que saber que item esta equipado actualmente
func update_equiped():
	for i in range(0,4):
		$lista_items/VBoxContainer.get_child(i).disable_equiped()
		
		if global.side_player.arma!=null:
			if $lista_items/VBoxContainer.get_child(i).get_node('name').text==global.side_player.arma.nombre:
						$lista_items/VBoxContainer.get_child(i).enable_equiped()

#funcion que equipa un arma del menu
func equip():
	#si el inventario no esta vacio (esto es por que si no hay nada tiraria error al equipar null)
	if !global.side_player.inv_arma.empty():
		#si estas en la ultima pagina
		if page_selected==ceil(global.side_player.inv_arma.size()/4.0)-1:
			#lo que viene sirve para determinar que si lo que seleccionaste de la ultima pagina no es null ya que esta
			#puede tener 1, 2, 3 o 4 items, se checkea el seleccionado contra el maximo posible en la pagina
			#si el item seleccionado es menor o igual al tamaño (total-1)%4
			if item_selected<=(global.side_player.inv_arma.size()-1)%4:
				global.side_player.arma=global.side_player.inv_arma[item_selected+(page_selected*4)]
				signal_manager.emit_signal('equip_arma')
		
		else:
			global.side_player.arma=global.side_player.inv_arma[item_selected+(page_selected*4)]
			signal_manager.emit_signal('equip_arma')
#al abrir se cargan los items y se updatean las flechas y el numero de pagina
func open():
	load_items()
	update_arrow()
	update_page_number()
	update_equiped()
	visible=true
	status=true

#al cerrar se resetea todo y se vuelve a la ventana anterior
func close():
	visible=false
	status=false
	item_selected=0
	page_selected=0
	get_parent().open_main()
	
#actualiza el numero de la pagina actual 
func update_page_number():
	$lista_items/VBoxContainer/numero_pagina.text='< '+String(page_selected)+' >'