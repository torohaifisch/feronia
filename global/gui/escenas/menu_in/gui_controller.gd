extends CanvasLayer


func _ready():
	#se emite estas señales atraves de signal manager
	signal_manager.connect('activate_vendor',self,'_on_vendor_activate')
	signal_manager.connect('vendedor_pociones',self,'_on_vendedor_pociones')
	
	signal_manager.connect('activate_inventory',self,'_on_inventory_activate')
	signal_manager.connect('money_change',self,'_on_money_change')
	signal_manager.connect('pociones_change',self,'_on_pociones_change')
	signal_manager.connect('transicion_a_batalla',self,'_on_transicion_a_batalla')
	signal_manager.connect('transicion_a_sidescroll',self,'_on_transicion_a_sidescroll')
	signal_manager.connect('activate_options',self,'_on_options_activate')
	signal_manager.connect('game_over_b',self,'_on_game_over')
	signal_manager.connect('save_game',self,'_on_save_game')
	signal_manager.connect('altar_gui',self,'_on_altar_gui')
	
	$Sprite.visible=true
#abre la gui del vendedor
func _on_vendor_activate(vendedor):
	signal_manager.deactivate_player()
	$vendedor_gui.open(vendedor)
	
#abre la gui del inventario
func _on_inventory_activate():
	signal_manager.deactivate_player()
	$menu_gui.open()
	
func _on_options_activate():
	signal_manager.deactivate_player()
	$options_gui.open()

func _on_altar_gui():
	signal_manager.deactivate_player()
	$Panel.open()

#actualiza el dinero 
func _on_money_change():
	$oro.text='Oro: '+String(global.side_player.monedas)

# actualiza pociones
func _on_pociones_change():
	$potas.text='x '+String(global.side_player.pociones)

func _on_transicion_a_batalla():
	$AnimationPlayer.play('transicion_batalla')
	get_tree().paused = true
	
func _on_transicion_a_sidescroll():
	$AnimationPlayer.play('transicion_sidescroll')

func _on_AnimationPlayer_animation_finished( anim_name ):
	#Pasar a Batalla
	if anim_name=='transicion_batalla':
		global.battle_mode()

	#Salir al morir
	if anim_name=='gameover':
		#global.start_mode(self,1)
		#get_tree().quit()
		$game_over_menu.open()
		
func _on_game_over():
	$AnimationPlayer.play('gameover')

func _on_save_game():
	$AnimationPlayer.play('save')

func _on_vendedor_pociones():
	signal_manager.deactivate_player()
	$pociones_gui.open()