extends Container


var status= false
var state= false
var last=1
var item_selected=0

func _ready():

	set_physics_process(true)
	visible=false
	signal_manager.connect('equip_arma',self,'_on_equip_arma')
	signal_manager.connect('equip_armadura',self,'_on_equip_armadura')
	signal_manager.connect('update_lvl',self,'_on_lvl_up')
func _physics_process(delta):
	if status==true:
		check_input()

#checkea los inputs posibles
func check_input():
#	if Input.is_action_just_pressed('ui_down'):
#		if item_selected==last:
#			item_selected=0
#		else:
#			item_selected+=1
#		update_arrow()
#
#	if Input.is_action_just_pressed('ui_up'):
#		if item_selected==0:
#			item_selected=last
#		else:
#			item_selected-=1
#		update_arrow()
		
		
	if Input.is_action_just_pressed('cancel'):
		close()
	
	if Input.is_action_just_pressed('attack'):
		$menu/AnimationPlayer.play('close')
		status=false
		
#activa/desactivar flechas
func update_arrow():
	disable_arrow()
	enable_arrow()
# activa flechas
func enable_arrow():
	$menu/VBoxContainer.get_child(item_selected).enable_arrow()

# desactiva flechas
func disable_arrow():
	for i in range(0,2):
		$menu/VBoxContainer.get_child(i).disable_arrow()
# actualiza atributos del jugador
func update_attr():
	
	if global.side_player.arma!=null:
		$info/VBoxContainer/dmg.text="Ataque: "+String(global.side_player.arma.dano)
	if global.side_player.armadura!=null:
		$info/VBoxContainer/def.text="Defensa: "+String(global.side_player.armadura.def)

func update_lvl():
	$info/VBoxContainer/nivel.text='Nivel: '+str(global.side_player.nivel)


func open():
	if state==false:
		state=true
		update_arrow()
		update_attr()
		visible=true
		
		$menu/AnimationPlayer.play('open')
	
#se resetea todo a default al cerrar y se activa el jugador
func close():
	
	visible=false
	state=false
	status=false
	item_selected=0
	signal_manager.activate_player()

# abre el menu principal al devolverse de los menu interiores(menu arma/armadura)
func open_main():
	$menu/AnimationPlayer.play('open')

func _on_equip_arma():
	
	update_attr()

func _on_equip_armadura():
	update_attr()
	
func _on_lvl_up():
	update_lvl()
	
func _on_AnimationPlayer_animation_finished( anim_name ):
	if anim_name=='open':
		status=true
		
	if anim_name=='close':
		if item_selected==0:
			$arma.open()
		#else:
		#	$armadura.open()