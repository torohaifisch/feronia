extends Container


var status= false
var state= false
var last=1
var item_selected=0

func _ready():

	set_physics_process(true)
	visible=false


func _physics_process(delta):
	if status==true:
		check_input()

#checkea los inputs posibles
func check_input():
	if Input.is_action_just_pressed('ui_down'):
		if item_selected==last:
			item_selected=0
		else:
			item_selected+=1
		update_arrow()
		
	if Input.is_action_just_pressed('ui_up'):
		if item_selected==0:
			item_selected=last
		else:
			item_selected-=1
		update_arrow()
		
		
	if Input.is_action_just_pressed('cancel') or Input.is_action_just_pressed('opciones'):
		close()
	
	if Input.is_action_just_pressed('attack'):
		$options/AnimationPlayer.play('close')
		status=false
		
#activa/desactivar flechas
func update_arrow():
	disable_arrow()
	enable_arrow()
# activa flechas
func enable_arrow():
	$options/VBoxContainer.get_child(item_selected).enable_arrow()

# desactiva flechas
func disable_arrow():
	for i in range(0,2):
		$options/VBoxContainer.get_child(i).disable_arrow()

func open():
	if state==false:
		state=true
		update_arrow()
		visible=true
		
		$options/AnimationPlayer.play('open')
	
#se resetea todo a default al cerrar y se activa el jugador
func close():
	
	visible=false
	state=false
	status=false
	item_selected=0
	signal_manager.activate_player()

# abre el menu principal al devolverse de los menu interiores(menu arma/armadura)
func open_main():
	$options/AnimationPlayer.play('open')

	
func _on_AnimationPlayer_animation_finished( anim_name ):
	if anim_name=='open':
		status=true
		
	if anim_name=='close':
		if item_selected==0:
			$controles.open()
		else:
			get_tree().quit()