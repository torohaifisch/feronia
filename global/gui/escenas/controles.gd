extends PanelContainer

var status= false

func _ready():

	set_physics_process(true)
	visible=false

func _physics_process(delta):
	if status==true:
		check_input()


func check_input():

	if Input.is_action_just_pressed('cancel'):
		close()
	

func open():


	get_parent().get_node('options/AnimationPlayer').play("open_control")

#al cerrar se resetea todo y se vuelve a la ventana anterior
func close():

	status=false
	get_parent().get_node('options/AnimationPlayer').play("close_control")

	


func _on_AnimationPlayer_animation_finished( anim_name ):
	if anim_name=='close_control':
		visible=false
		get_parent().open_main()
	if anim_name=='open_control':
		status=true