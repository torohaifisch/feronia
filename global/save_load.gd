extends Node

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
const SAVE_PATH='user://save.json'
func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

func save_game():
	var save_dict={}
	save_dict['side_scene']=global.sidescroll_scene.get_filename()
	var nodes= get_tree().get_nodes_in_group('persistente')
	for node in nodes:
		save_dict[node.get_name()]=node.save_state()
	
	var save_file=File.new()
	save_file.open(SAVE_PATH,File.WRITE)
	
	save_file.store_line(to_json(save_dict))
	
	save_file.close()
	
func load_game(root_scene):
	var save_file=File.new()
	if !save_file.file_exists(SAVE_PATH):
		if root_scene.name=='menu_principal':
			return
		global.deferred_no_save(global.sidescroll_scene,0)
		return
	save_file.open(SAVE_PATH,File.READ)
	var current_line
	current_line=parse_json(save_file.get_line())
	print(current_line)
	global.load_scene(root_scene,current_line['side_scene'],current_line['Player'])

func delete_save():
	var dir=Directory.new()
	if dir.file_exists(SAVE_PATH):
		dir.remove(SAVE_PATH)