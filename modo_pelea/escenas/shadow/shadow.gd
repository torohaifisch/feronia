extends Sprite

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var ray_up
var ray_down
var ray_left
var ray_right

func _ready():
	
	ray_up=$ray_up
	ray_down=$ray_down
	ray_left=$ray_left
	ray_right=$ray_right
	

func ray_collide(dir):
	if dir=='up' and ray_up.is_colliding():
		return true
	elif dir=='down' and ray_down.is_colliding():
		return true
	elif dir=='left' and ray_left.is_colliding():
		return true
	elif dir=='right' and ray_right.is_colliding():
		return true