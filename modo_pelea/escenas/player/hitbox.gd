extends Area2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

func activate():
	visible=true
	monitoring=true
	$Timer.start()

func _on_Timer_timeout():
	monitoring=false
	visible=false
