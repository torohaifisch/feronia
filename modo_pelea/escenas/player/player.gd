extends KinematicBody2D

# Class controlling the player

# Movement Constants
const GRAVITY = 650.0
const WALK_MAX = 160
const STOP_FORCE = 1300
const STOP_COEFF = 0.65

const JUMP_VEL = 300#280
const MAX_AIR = 0.11 # Due to physics, character is always in the air. This is a tolerance


# Variables


# Estados
var velocity = Vector2()
var air_time = 100
var jumping = false
var can_jump = true
var falling = false

var attacking = false # player is attacking
var can_attack = true

var hit=false
var knockback=false
var knockingback=false
var standing=false
var invincible=false


var dashing=false

# Input
var walk_left
var walk_right
var walk_up
var walk_down
var jump
var attack
var cancel
var shoot
var change

# Spritework
var sees_left = false
var landed = true
var stopped = false
var player_sprite

#player vars
var hp=5
var dmg

# Death
var dead = false
var active=false
#mirroring
var sprite

var shadow_res=preload("res://modo_pelea/escenas/shadow/shadow.tscn")
var shadow
#enemy
#lista de enemigos que nos atacan
var enemy_list = []
#enemigo actual al que le pegamos
var current_enemy=null

func _ready():
	
	set_physics_process(true)
	set_process_input(true)
	
	player_sprite = $container/PlayerSprite/PlayerAnim
	player_sprite.play("pre_idle")
	
	if shadow==null:
		sprite=$container
		shadow=shadow_res.instance()
		get_parent().add_child(shadow)
		
	
	
func _physics_process(delta):
	
	walk_left = Input.is_action_pressed("ui_left")
	walk_right = Input.is_action_pressed("ui_right")
	walk_up = Input.is_action_pressed("ui_up")
	walk_down = Input.is_action_pressed("ui_down")
	jump = Input.is_action_pressed("jump")



	if(knockback):
	#posicion de la sombra
		shadow.global_position.x=global_position.x
		
		knockback(delta)
	
		#Cuando el knockback termina
		if ( global_position.y+41>=shadow.global_position.y):
		
			global_position=shadow.global_position-Vector2(0,40)
			knockback = false
			knockingback=false
			
			if(hp<=0):
				# ACA MUERE
				
				player_sprite.play('death')
				signal_manager.game_over_b()
			
			else:
				standing = true
				
				velocity=Vector2(velocity.x,0)
				
				player_sprite.play("stand")
		#posicion de la sombra

	#---------------Roll--------------
	if (not dead and active):
		if Input.is_action_just_pressed('roll') and !dashing and !hit and !knockback and !standing and !is_on_air():
			dashing=true
			invincible=true
			player_sprite.play('roll')
		
		if !player_sprite.is_playing('roll'):
			dashing=false
			invincible=false
		
		if dashing:
			var force=Vector2(0,0)
			if sees_left and !shadow.ray_collide("left"):
				force.x=-1
			elif !sees_left and !shadow.ray_collide("right"): 
				force.x=1
			velocity.x=300*force.x
			var motion = move_and_slide(velocity)
	
	if (not dead and active and !hit and !knockback and !standing):
		_movement(delta)

		
		
func _input(event):
	
		attack = event.is_action_pressed("attack")
		cancel= event.is_action_pressed('cancel')
		
	
func _movement(delta):

	var force = Vector2(0,0)
	
	# stop by default, inertia
	var stop = true
	# -----------MOVIMIENTO------------
	if (!is_attacking() and !dashing):
		if (walk_left and !shadow.ray_collide("left")  and !walk_right):
				force+= Vector2(-1,0)
				
				stop = false
				stopped = false
				
				if (!player_sprite.is_playing("run_b")  and !is_on_air() && !attacking):
					player_sprite.play("run_b")
			
		if (walk_right and !walk_left):
	
			
				force+= Vector2(1,0)
				stop = false
				stopped = false
				
				if (!player_sprite.is_playing( "run_b") and !is_on_air() && !attacking):
					player_sprite.play("run_b")
	
		if (walk_up and !walk_down and !shadow.ray_collide("up")):
	
			
				force+= Vector2(0,-1)
			
				stop = false
				stopped = false
			
				if (!player_sprite.is_playing( "run_b")  and !is_on_air() && !attacking):
					player_sprite.play("run_b")
		
		if (walk_down and !walk_up and !shadow.ray_collide("down")):
	
			
				force+= Vector2(0,1)
				
				stop = false
				stopped = false
				if (!player_sprite.is_playing("run_b") and !is_on_air() && !attacking):
					player_sprite.play("run_b")
	
	
	if stop==true:
		stop()
	# Manage jumping
	if falling:
		if ( global_position.y+41>=shadow.global_position.y):

			global_position=shadow.position-Vector2(0,40)
			falling = false
			
			if (not landed):
				velocity=Vector2(velocity.x,0)
				landed = true
				player_sprite.play("idle_2")
				

	#-------------- CAMBIO DE JUMP A FALL -------------
	if (jumping and velocity.y>=0):
		
		falling = true
		jumping=false
		
	# ------------------CONTROL ANIMACION JUMP FALL ---------------
	if (velocity.y < 0 and !player_sprite.is_playing("jump") and jumping and !attacking):
		
		player_sprite.play("jump")
		player_sprite.seek(0.3)
		

	if (falling and !player_sprite.is_playing("fall") and !attacking):
		
		player_sprite.play("fall")
	
	
	# -------------------SALTO-------------------
	if (!jump):
		# puedo saltar si no he apretado el boton de saltar
		can_jump = true
	
	#si no estoy saltando y puedo saltar y aprete para saltar y no estoy callendo o atacando
	if (not jumping and can_jump and jump and !falling  and !attacking and !dashing):
		
		landed=false
		can_jump = false
		jumping=true
		
		player_sprite.play("jump")
		
		velocity.y = -JUMP_VEL
	
	if (is_on_air()):
		#se suman las fuerzas en el eje x de velocidad y la gravedad
		force.x= force.x*WALK_MAX
		force += Vector2(0, GRAVITY)
		
		# si ataco en el aire es necesario que el movimiento continue por lo que se suman las velocidades
		if is_attacking():
			velocity += force * delta
			if(shadow.ray_collide("left") or shadow.ray_collide("right")):
				velocity.x=0
			
		# si no estoy atacando entonces puedo moverme libremente
		else:
			#esto es movimiento sin acelerar en el eje x por lo que la velocidad se iguala en x
			velocity.x=force.x
			# aqui es con aceleracion por la gravedad la velocidad tiene que sumarse a las anteriores para el eje y
			velocity.y+=force.y*delta
		
		# esto regula la velocidad al moverte mientras saltas
		if velocity.x>200 or velocity.x<-200:
			velocity.x=200*sign(velocity.x)
	
	else:	
			#si no estoy saltando entonces el movimiento es normal para todas las direcciones
			velocity = force.normalized()* WALK_MAX
			
	
	#-----------------MOVIMIENTO-------------
	var motion = move_and_slide(velocity)
	
	
	#----------------ATAQUE---------------
	if(!attack and !cancel):
		
		can_attack=true
		
	elif(can_attack && !attacking):
		
		if attack and walk_up:
#			can_attack=false
#			special(1)
			pass
			
		elif attack:
			
			can_attack=false
			attack()
		elif cancel:
			
			can_attack=false
			attack2()
	
	
	# puedo atacar si la animacion termina
	if (attacking and !is_attacking()):
		
		attacking=false
		#desactiva hitbox jump attack
		$container/hitboxes/jump_attack.deactivate()
		
	# -----------SPRITE MIRRORING---------------
	if (velocity.x>0):
		sees_left = false
	elif (velocity.x<0):
		sees_left = true
	if (sees_left):
		sprite.scale.x=-1
	else:
		sprite.scale.x=1

	# ----------- POSICION SHADOW ----------
	if is_on_air():
		
		shadow.position.x=global_position.x
	else:
		shadow.position=global_position+Vector2(0,40)
	
	

	

	
	
#------------FUNCIONES-----------------

#FUncion de Muerte
func death():
	pass

# knockback 
func knockback(delta):
	
	var force=Vector2(1,0)
	if !player_sprite.is_playing("knockback"):
		player_sprite.play("knockback")
	if !knockingback:
		velocity.y = -JUMP_VEL
		knockingback=true
	#se suman las fuerzas en el eje x de velocidad y la gravedad
	force += Vector2(0, GRAVITY)
	
	# si ataco en el aire es necesario que el movimiento continue por lo que se suman las velocidades
	
	velocity += force * delta
	velocity.x=200

	#-----------------MOVIMIENTO-------------
	var motion = move_and_slide(velocity)

func stop():
	
	if velocity==Vector2(0,0):
		if (!player_sprite.is_playing("idle_2") && !is_on_air() &&!is_attacking()):
			
			if (not stopped and !is_on_air()):
				
				stopped = true
				player_sprite.play("idle_2")


#----------------Funciones de ataque--------------
# combo de ataques normal
func attack():
		
		attacking = true
		if is_on_air():
			player_sprite.play("jump_attack")
		elif !jumping:
				
			if (!player_sprite.is_playing("combo1_1") and !player_sprite.is_playing("combo1_2")
			 and !player_sprite.is_playing("combo1_3") and !player_sprite.is_playing("combo2_1")):
				
				player_sprite.play("combo1_1")
				if (sees_left == false):
					global_position+=Vector2(+8,0)
				else:
					global_position+=Vector2(-8,0)
			
			elif (player_sprite.is_playing("combo1_1") or player_sprite.is_playing('combo2_1')):
				player_sprite.play("combo1_2")
				if (sees_left == false):
					global_position+=Vector2(+8,0)
				else:
					global_position+=Vector2(-8,0)
			
			elif (player_sprite.is_playing('combo1_2') or player_sprite.is_playing('combo2_2')):
				player_sprite.play("combo1_3")

func attack2():
	
	attacking = true

	if !is_on_air():
			
		if (!player_sprite.is_playing("combo2_1") and !player_sprite.is_playing("combo2_2") and !player_sprite.is_playing("combo1_1")):
			player_sprite.play("combo2_1")
			if (sees_left == false):
				global_position+=Vector2(+8,0)
			else:
				global_position+=Vector2(-8,0)

		elif (player_sprite.is_playing("combo2_1") or player_sprite.is_playing('combo1_1')):
			player_sprite.play("combo2_2")
			if (sees_left == false):
				global_position+=Vector2(+8,0)
			else:
				global_position+=Vector2(-8,0)
		
		elif (player_sprite.is_playing( "combo2_2") or player_sprite.is_playing('combo1_2')):
				player_sprite.play("combo2_3")

func slow_attack():
	attacking=false

func special(n):
	attacking = true
	
	if !is_on_air():
	
		if n==1:
			if !player_sprite.is_playing('special_1'):
				player_sprite.play('special_1')
		
func spawn_latigo():
	var latigo=load('res://modo_pelea/escenas/player/especiales/latigo_proyectil.tscn')
	var l_inst=latigo.instance()
	l_inst.global_position=$container/Position2D.global_position
	if sees_left:
		l_inst.set_signo(-1)
		l_inst.scale.x=-2
	else:
		l_inst.set_signo(1)
	get_parent().add_child(l_inst)
	
# -------------------HELPERS-------------------

#funcion para checkear si el personaje esta atacando
func is_attacking():
	if (player_sprite.is_playing("combo1_1") or player_sprite.is_playing("combo1_2") or player_sprite.is_playing("combo1_3") or
	player_sprite.is_playing("jump_attack") or player_sprite.is_playing("combo2_1") or player_sprite.is_playing("combo2_2") or 
	player_sprite.is_playing("combo2_3") or player_sprite.is_playing("special_1")):
		return true
	else:
		return false
# checkea si el player esta en el aire
func is_on_air():
	if jumping or falling:
		return true
	
	return false

func allow_attack(enemy):
	if enemy_list.size()<1:
		enemy_list.append(enemy)
		return true
	return false

func activate():
	active=true

func stand_end():
	standing=false
	
#cuando es golpeado
func hit():

	if !invincible:
		#si la hp es mayor a 0 play hit si no seteas knockback a true
		hit=true
		
		if hp>0 and !knockback and !standing:
			
			hp-=1
			signal_manager.update_hp(hp)
			if hp>0:
				randomize()
				var ran=rand_range(0,1)
				if is_on_air():
					knockback=true
					hit=false
				if ran>0.5:
					player_sprite.play('hit1')
				else:
					player_sprite.play('hit2')
			else:
				
				hit=false
				if dead==false:
					dead=true
					knockback=true
		
#cuando la animacion de hit termina se setea hit a falso
func stop_hit():
	hit=false


func activate_hitbox(name):
	if name =='1_1':
		$container/hitboxes/combo1_1.activate()
	if name=='1_2':
		$container/hitboxes/combo1_2.activate()
	if name=='1_3':
		$container/hitboxes/combo1_3.activate()
	if name=='2_1':
		$container/hitboxes/combo2_1.activate()
	if name=='2_2':
		$container/hitboxes/combo2_2.activate()
	if name=='2_3':
		$container/hitboxes/combo2_3.activate()
	if name=='jump_attack':
		$container/hitboxes/jump_attack.activate()
		
func set_pos(p):
	global_position=p
	shadow.position=global_position+Vector2(0,40)

#------------------Funciones de señales----------------------


func _on_PlayerAnim_animation_finished( anim_name ):
	if !dead:
		player_sprite.play("idle_2")



func _on_combo_hitbox_body_entered( body ):
	if body.is_in_group('basic_enemy'):
		current_enemy=body
		
		if player_sprite.is_playing('jump_attack'):
			body.knockback=true
		else:
			body.hit()
		signal_manager.enemy_info(body)
