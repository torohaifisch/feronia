extends KinematicBody2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var signo=1
func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	set_physics_process(true)
	$AnimationPlayer.play("animate")

func _physics_process(delta):
	position.x+=5*signo
#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
func set_signo(n):
	signo=n

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
