extends CanvasLayer

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	signal_manager.connect('enemy_info',self,'_on_emit_info')
	signal_manager.connect('game_over_b',self,'_on_game_over')
	signal_manager.connect('lvl_up',self,'_on_lvl_up')
	
	$resultados.visible=false
	$resultados/Panel/VBoxContainer/HBoxContainer/expe.visible=false
	$resultados/Panel/VBoxContainer/HBoxContainer2/mon.visible=false
	
	
	#---Debugger----
	set_physics_process(true)
func _physics_process(delta):
	if Input.is_action_just_pressed('debug'):
		if !$Panel.visible:
			$Panel.visible=true
		else: 
			$Panel.visible=false
	if global.player!=null:
		$Panel/Label.text='jumping: '+String(global.player.jumping) + '\nfalling: '+String(global.player.falling) + '\nknockback: '+ String(global.player.knockback) + '\nattacking: '
		$Panel/Label.text+= String(global.player.attacking) + '\ninvincible: '+ String(global.player.invincible)+ '\nstanding:'
		$Panel/Label.text+=String(global.player.standing)+'\ndashing: '+String(global.player.dashing)+'\nhit: '+String(global.player.hit)

func _on_game_over():
	$AnimationPlayer.play('gameover')
	
func show_result(e,m):
	$resultados.visible=true
	fill_result(e,m)
	$AnimationPlayer.play('results')
func fill_result(e,m):
	$resultados/Panel/VBoxContainer/HBoxContainer/expe.text='Experiencia: '+String(e)
	$resultados/Panel/VBoxContainer/HBoxContainer2/mon.text='Monedas: '+String(m)
	
func _on_emit_info(body):
	$enemy_info/Panel/nombre.text=body.nombre
	$enemy_info/Panel/vida.text='Hp: '+String(body.hp)

func _on_lvl_up():
	$AnimationPlayer2.play("lvl_up")

