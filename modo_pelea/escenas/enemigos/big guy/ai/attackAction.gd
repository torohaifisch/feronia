extends "res://addons/godot-behavior-tree-plugin/bt_base.gd"

# Leaf Node
func tick(tick):
	if !tick.actor.is_attacking():
		tick.actor.play_combo_1()
	
	return OK
