extends "res://addons/godot-behavior-tree-plugin/bt_base.gd"

# Leaf Node
func tick(tick):

	if tick.actor.is_attacking():
		return FAILED
	else:	
		tick.actor.move()
		return OK
