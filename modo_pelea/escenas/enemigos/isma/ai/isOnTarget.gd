extends "res://addons/godot-behavior-tree-plugin/bt_base.gd"

# Leaf Node
func tick(tick):
	if tick.actor.is_on_target():
			
			return OK

	return FAILED
