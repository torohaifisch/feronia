extends "res://addons/godot-behavior-tree-plugin/bt_base.gd"

# Leaf Node
func tick(tick):
	tick.actor.aggro += .75
	randomize()
	if(rand_range(0,1) < tick.actor.aggro*.05):  
		return OK
	#print(false)
	return FAILED