extends Area2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var damage
func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	visible=false
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
func activate(d):
	visible=true
	damage=d
	monitoring=true
	$Timer.start()

func _on_Timer_timeout():
	monitoring=false
	visible=false

func _on_attack_body_entered( body ):
	if body.name=='Player':
	
		global.player.hit()