extends KinematicBody2D

const GRAVITY = 650.0
const JUMP_VEL=300
const WALK_MAX = 200
const base_exp= 50
const base_moneda=2
const nombre='Fish'


var nivel=1
var hp=5
var dmg=2


var real_exp
var real_moneda

# arbol ai
var blackboard
var behavior_tree
var aggro=0
var animator
var target

var state
var active=false

# para hacer mirror del sprite
var container

# knock -> stand
var knockback=false
var standing=false
var knockingback=false
var hit=false
var dead=false
var velocity=Vector2()

#sombra
var shadow_res=preload("res://modo_pelea/escenas/shadow/shadow.tscn")
var shadow
var shadow_offset=Vector2(0,25)
var limites={}




func _ready():

	blackboard=$BehaviorBlackboard
	behavior_tree=$BehaviorTree
	animator=$container/AnimatedSprite/EnemyAnim
	container=$container
	
	state='random_move'

	if shadow==null:

		shadow=shadow_res.instance()
		get_parent().add_child(shadow)
		shadow.z_index=z_index-1
		shadow.global_position=global_position+shadow_offset
	
	
	set_physics_process(true)
	
func _physics_process(delta):
	
	if(knockback):
		#posicion de la sombra
		shadow.global_position.x=global_position.x
		
		knockback(delta)
	
		#Cuando el knockback termina
		if ( global_position.y+shadow_offset.y+1>=shadow.global_position.y):
		
			global_position=shadow.global_position-shadow_offset
			knockback = false
			knockingback=false
			
			if(hp<=0):
				# ACA MUERE
				
				animator.play('death')
			
			else:
				standing = true
				
				velocity=Vector2(velocity.x,0)
				
				animator.play("stand")
	#posicion de la sombra
	else:
		shadow.global_position=global_position+shadow_offset
		
	
	if(!knockback and !standing and !hit and !dead and active):
		behavior_tree.tick(self,blackboard)
	


#--------------------------Funciones AI----------------
# Si el jugador esta en rango
func is_player_in_range():
	if abs(shadow.global_position.x-global.player.shadow.global_position.x)<70 and abs(shadow.global_position.y-global.player.shadow.global_position.y)<15:
		return true
	return false

# ckeckea si el enemigo esta atacando
func is_attacking():
	if (animator.get_current_animation() == "attack"):
		return true
	else:
		return false

# funcion para moverse
func move():
	var vector= target-shadow.global_position
	if state=='attacking':
		WALK_MAX=260
	else:
		WALK_MAX=200
	move_and_slide(vector.normalized()*WALK_MAX)
	set_mirroring()
		
	if animator.get_current_animation()!="move":
		animator.play("move")

#checkea si esta en el target
func is_on_target():
	if (target.x-5<=shadow.global_position.x and shadow.global_position.x<=target.x+5):
		
		if (target.y-5<=shadow.global_position.y and shadow.global_position.y<=target.y+5):
			
			return true
	
	return false

#encuentra un target para atacar
func find_attack_target():
	var lado
	lado=sign(global.player.global_position.x-global_position.x)*-1
	var rand1=lado*60
	randomize()
	var rand2=rand_range(0,4)
	target=global.player.shadow.global_position+Vector2(rand1,rand2)
	

func play_idle():
	if animator.get_current_animation()!="idle" and !is_attacking():
		animator.play("idle")
		
	
func play_combo_1():
	if animator.get_current_animation()!="attack":
		animator.play("attack")

func set_mirroring():
	var vector= global.player.global_position-global_position	
	container.scale.x=sign(vector.x)*1
	

# encuentra target random para moverse
func find_random_point():
	var signo
	var signo2
	var point
	remove_player_list()
	play_idle()

	
	randomize()
	var rand1=rand_range(0,1)
	
	if rand1>0.5:
		signo=1
	else: 
		signo=-1
	
	randomize()
	rand1=rand_range(0,1)
	if rand1>0.5:
		signo2=1
	else:
		signo2=-1
	randomize()
	rand1=rand_range(60,250)
	
	
	point=shadow.global_position+Vector2(signo*rand1,signo2*10)
	if point.y<limites['up'].y:
		point.y=limites['up'].y
		
	if point.y>limites['down'].y:
		point.y=limites['down'].y

	
	target=point
	
	return true

#cuando es golpeado
func hit():

	if !standing:
		#si la hp es mayor a 0 play hit si no seteas knockback a true
		hit=true
		
		if hp>0:
			
			if global.side_player.arma!=null:
				hp-=global.side_player.arma.dano
			
			else:
				hp-=global.side_player.dano
			
			if hp<0:
				hp=0
			
			if hp>0:
	
				animator.play('hit')
	
			else:
				
				hit=false
				if dead==false:
					dead=true
					knockback=true
		
#cuando la animacion de hit termina se setea hit a falso
func stop_hit():
	hit=false
	
# knockback 
func knockback(delta):
	
	var force=Vector2(1,0)
	if animator.get_current_animation()!="knockback":
		animator.play("knockback")
	if !knockingback:
		velocity.y = -JUMP_VEL
		knockingback=true
		state='random_move'
		target=shadow.global_position
		remove_player_list()
	#se suman las fuerzas en el eje x de velocidad y la gravedad
	force += Vector2(0, GRAVITY)
	
	# si ataco en el aire es necesario que el movimiento continue por lo que se suman las velocidades
	
	velocity += force * delta
	velocity.x=200*sign(global_position.x-global.player.global_position.x)
	#if(shadow.ray_collide("left") or shadow.ray_collide("right")):
	#	velocity.x=0
		
	
	#-----------------MOVIMIENTO-------------
	var motion = move_and_slide(velocity)


# se activa al terminar la animacion de pararse
func activate():
	play_idle()
	target=global_position+shadow_offset
	standing=false

func remove_player_list():
	var me=global.player.enemy_list.find(self,0)
	if me!=-1:
		global.player.enemy_list.remove(me)

# cambia de estados
func change_state():
	if state=='random_move':
	
		randomize()
		var rand1=rand_range(0,1)
		
		if rand1>0.5:
			if global.player.allow_attack(self):
				state='attacking'
				return true
		
		else: 
			return false
	
	elif state=='attacking':
		
	
		
		state='random_move'
	
		return true
#----------------------FUNCIONES------------------
#FUncion de Muerte
func death():
	#De esta manera es independiente del lugar en el que esta el nodo en el arbol al tener en global la escena de beatemup
	global.beat_scene.remove_enemy()
	global.beat_scene.add_money(real_moneda)
	global.beat_scene.add_exp(real_exp)

	#-----------------
	shadow.queue_free()
	queue_free()
	
func set_active(x):
	active=x
	
# configuracion inicial de enemigo
func setup(lvl,limites=null):
	randomize()
	global_position=Vector2(rand_range(0,1024),rand_range(300,550))
	nivel=lvl
	real_exp=base_exp*nivel
	real_moneda=base_moneda*nivel
	target=global_position+shadow_offset
	set_limites(limites)

func attack(n):
	if n==1:
		$container/hitboxes/attack.activate(dmg)

# diccionario con puntos limite del nivel de pelea
func set_limites(l):
	limites=l

