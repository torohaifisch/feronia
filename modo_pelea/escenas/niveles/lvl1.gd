extends Node2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var ene
var cantidad_enemigos=0
var monedas=0
var experiencia=0
var end=false
# diccionario de enemigos, cuidar el orden de estos
var enemy_dict= {
	1:"res://modo_pelea/escenas/enemigos/enemy_test/enemy.tscn",
	2:"res://modo_pelea/escenas/enemigos/cakou/cakou.tscn",
	3:"res://modo_pelea/escenas/enemigos/fish/fish.tscn",
	4:"res://modo_pelea/escenas/enemigos/conejo/conejo.tscn",
	5:"res://modo_pelea/escenas/enemigos/big guy/big_guy.tscn",
	6:"res://modo_pelea/escenas/enemigos/isma/isma.tscn"
}
var bg_dict= {
	1:"res://modo_pelea/sprites/lvl1/background_beatemup1.png",
	2:"res://modo_pelea/sprites/lvl1/background_beatemup2.png",
	3:"res://modo_pelea/sprites/lvl1/background_beatemup3.png",
}

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	
	set_physics_process(true)
	
	global.spawn_player(Vector2(350,300))
	
	$CanvasLayer/AnimationPlayer.play("fight")
	
func _physics_process(delta):

	global.sort_z()
	
	if cantidad_enemigos==0 and end==false:
		end =true
		global.side_player.hp=global.player.hp
		global.side_player.gain_exp(experiencia)
		global.side_player.monedas+=monedas
		$CanvasLayer.show_result(experiencia,monedas)


func spawn_enemies(enemigo,cantidad,nivel):
	print(enemigo)
	cantidad_enemigos+=cantidad
	for i in cantidad:
		create_enemy(enemigo,nivel)

func create_enemy(enemy,lvl):

	ene= load(enemy_dict[int(enemy)])
	
	var enemigo=ene.instance()
	
	add_child(enemigo)
	
	var limites=limites_dict()
	
	enemigo.setup(lvl,limites)
	
func remove_enemy():
	cantidad_enemigos-=1
	
func add_money(m):
	monedas+=m
	
func add_exp(xp):
	experiencia+=xp

# activa al player y los enemigos del nivel
func activate():
	global.player.activate()
	get_tree().call_group("basic_enemy", "set_active",true) 

func _on_AnimationPlayer_animation_finished( anim_name ):
	
	if anim_name=="fight":
		activate()
	
	elif anim_name=='gameover':
		$CanvasLayer/game_over_menu.open()
	
# se deuvelve al sidescroll, le pasa self para borrar el nivel beat em up
func go_back():
	global.beat_scene=null
	global.back_to_sidescroll(self)


#setea la textura del bg
func set_bg(n):
	if n==0:
		n=1
	$Sprite.texture=load(bg_dict[n])

func limites_dict():
	var dict={}
	var limites= $enemy_limit.get_children()
	dict['up']=limites[0].position
	dict['down']=limites[1].position
	dict['left']=limites[2].position
	dict['right']=limites[3].position
	return dict