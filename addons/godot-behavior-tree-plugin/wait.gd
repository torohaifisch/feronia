extends "res://addons/godot-behavior-tree-plugin/bt_base.gd"

export(float) var wait_time = 0.0

var last_time = 0.0

# Decorator Node
func tick(tick):
	
	last_time=last_time+get_physics_process_delta_time()

	tick.actor.play_idle()

	if last_time<wait_time:
	
		return ERR_BUSY
	else: 
		last_time=0.0
		return OK
