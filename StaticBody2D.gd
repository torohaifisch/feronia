extends StaticBody2D

# class member variables go here, for example:
var active = true
# var b = "textvar"
var root_sprite

func _ready():
	root_sprite = $AnimatedSprite/AnimationPlayer
	# Called when the node is added to the scene for the first time.
	# Initialization here

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
func open():
	if active == true: 
		root_sprite.play("deactivate")
		$CollisionShape2D.disabled=true
		$".".set_collision_layer_bit(10, false)#desactivar la colision en la capa 10 para que fantasma no la reconozca
		active = false
	elif active == false:
		root_sprite.play("activate")
		$CollisionShape2D.disabled=false
		$".".set_collision_layer_bit(10, true)
		active = true

func _on_AreaRoot_body_entered(body):
	if body.name=='Player':
		body.root=self


func _on_AreaRoot_body_exited(body):
	if body.name=='Player':
		body.root=null
