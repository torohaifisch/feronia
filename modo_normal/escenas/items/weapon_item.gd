extends Node2D

export (String) var arma

func _ready():
	pass


#si entra un cuerpo perteneciende al grupo player entonces el nodo se elimina
func _on_Area2D_body_entered( body ):
	if body.is_in_group("players"):
		queue_free()
