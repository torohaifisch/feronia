extends KinematicBody2D

export (int) var dificultad

export (Vector3) var vec
export(String, FILE) var file_path

func _ready():
	# Called every time the node is added to the scene.
	
	if global.side_player!=null:
		if global.side_player.experiencia!=0 or global.side_player.nivel>1:
			print('se borra')
			queue_free()
	
	
	
func activar():
	if global.side_player.invulnerable==false:
	#global.side_player.invulnerable=true
		global.current_enemy=self
		signal_manager.activate_player()
		signal_manager.transicion_a_batalla()

#al terminar el dialogo se activa el jugador

func end_dialogue():
	activar()





func _on_Area2D_body_entered( body ):
	if body.name=='Player':
	#Player se vuelve invulnerable para evitar re entrar a batalla automaticamente al salir de ella
		signal_manager.activate_dialogue(file_path,self)

