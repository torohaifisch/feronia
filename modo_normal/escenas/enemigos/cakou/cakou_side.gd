extends KinematicBody2D

export (int) var dificultad

export (Vector3) var vec


const GRAVITY=650
const SPEED=100
const FLOOR_NORMAL = Vector2(0, -1)
const SLOPE_SLIDE_STOP = 25.0
var velocity=Vector2()
var force=Vector2(-1,50)



func _ready():
	# Called every time the node is added to the scene.
	set_physics_process(true)
	var nodes=get_tree().get_nodes_in_group('puentes')
	for i in nodes:
		$left.add_exception(i)
		$right.add_exception(i)

func _physics_process(delta):
	#esto checkea los raycast izquierda y derecha para cambiar la fuerza debendiendo si no estan colisionando con el suelo
	# (-1 es movimiento hacia la izquierda y 1 es derecha)

	if force.x==-1: 
		if $left.is_colliding() and !$left_wall.is_colliding():
			
			force.x=-1
		else:
			force.x=1
	if force.x==1: 
		if $right.is_colliding() and !$right_wall.is_colliding():
			force.x=1
		else:
			force.x=-1
	
	if force.x==1:
		$AnimatedSprite.flip_h=true
	else:
		$AnimatedSprite.flip_h=false
	
			
	velocity = force *SPEED
	
	var motion = move_and_slide(velocity,FLOOR_NORMAL,SLOPE_SLIDE_STOP)

	if velocity.x !=0 and !$AnimationPlayer.is_playing('move'):
	
		$AnimationPlayer.play('move')
	if is_on_floor():
		velocity.y=0
func _on_Area2D_body_entered( body ):
	if body.name=='Player':
	#Player se vuelve invulnerable para evitar re entrar a batalla automaticamente al salir de ella
		if global.side_player.invulnerable==false:
			#global.side_player.invulnerable=true
			global.current_enemy=self
			
			signal_manager.transicion_a_batalla()