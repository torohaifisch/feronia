extends KinematicBody2D

export (int) var dificultad

export (Vector3) var vec


func _ready():
	# Called every time the node is added to the scene.
	pass
func _on_Area2D_body_entered( body ):
	if body.name=='Player':
	#Player se vuelve invulnerable para evitar re entrar a batalla automaticamente al salir de ella
		if global.side_player.invulnerable==false:
			#global.side_player.invulnerable=true
			global.current_enemy=self
			
			signal_manager.transicion_a_batalla()