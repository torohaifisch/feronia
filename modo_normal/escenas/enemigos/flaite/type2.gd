extends KinematicBody2D

const GRAVITY=650
const SPEED=150
const FLOOR_NORMAL = Vector2(0, -1)
const SLOPE_SLIDE_STOP = 25.0
var velocity=Vector2()
var force=Vector2(-1,0)

func _ready():
	# Called every time the node is added to the scene.
	set_physics_process(true)

func _physics_process(delta):
	#esto checkea los raycast izquierda y derecha para cambiar la fuerza debendiendo si no estan colisionando con el suelo
	# (-1 es movimiento hacia la izquierda y 1 es derecha)
	if force.x==-1: 
		if $left.is_colliding():
			
			force.x=-1
		else:
			force.x=1
	if force.x==1: 
		if $right.is_colliding():
			force.x=1
		else:
			force.x=-1
			
	velocity = force *SPEED

	var motion = move_and_slide(velocity,FLOOR_NORMAL,SLOPE_SLIDE_STOP)
	