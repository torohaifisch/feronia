extends KinematicBody2D

export (int) var dificultad

export (Vector3) var vec


const GRAVITY=300
const SPEED=230
const FLOOR_NORMAL = Vector2(0, -1)
const SLOPE_SLIDE_STOP = 25.0
var velocity=Vector2()
var force=Vector2(-1,0)
var player=null

func _ready():
	set_physics_process(true)




func _physics_process(delta):
	#esto checkea los raycast izquierda y derecha para cambiar la fuerza debendiendo si no estan colisionando con el suelo
	# (-1 es movimiento hacia la izquierda y 1 es derecha)
	velocity.x=0
	if player!=null:
		force.x=sign(player.position.x-position.x)
			
		velocity.x = force.x *SPEED
	
	velocity.y=GRAVITY
	var motion = move_and_slide(velocity,FLOOR_NORMAL,SLOPE_SLIDE_STOP)

	if velocity.x != 0 and !$AnimatedSprite/AnimationPlayer.is_playing('run'):
		$AnimatedSprite/AnimationPlayer.play('run')
	elif velocity.x == 0 and !$AnimatedSprite/AnimationPlayer.is_playing('idle'):
		$AnimatedSprite/AnimationPlayer.play('idle')
		
	if  force.x==1:
		$AnimatedSprite.flip_h=true
	if force.x==-1:
		$AnimatedSprite.flip_h=false
	#	$AnimationPlayer.play('move')
	if is_on_floor():
		velocity.y=0

func _on_Area2D_body_entered( body ):
	if body.name=='Player':
	#Player se vuelve invulnerable para evitar re entrar a batalla automaticamente al salir de ella
		if global.side_player.invulnerable==false:
			#global.side_player.invulnerable=true
			global.current_enemy=self
			signal_manager.transicion_a_batalla()

func _on_player_detector_body_entered(body):
	if body.name=='Player':
		#$AnimatedSprite/AnimationPlayer.play('run')
		player=body


func _on_player_detector_body_exited(body):
	if body.name=='Player':
		#$AnimatedSprite/AnimationPlayer.play('idle')
		player=null