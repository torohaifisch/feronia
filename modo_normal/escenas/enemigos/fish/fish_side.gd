extends KinematicBody2D

export (int) var dificultad
export (Vector3) var vec

var force=Vector2(0,-1)
var SPEED=250
const FLOOR_NORMAL = Vector2(0, -1)
const SLOPE_SLIDE_STOP = 25.0

func _ready():
	set_physics_process(true)
	
func _physics_process(delta):
				
	var velocity = force *SPEED
	
	var motion = move_and_slide(velocity,FLOOR_NORMAL,SLOPE_SLIDE_STOP)
	if !$AnimationPlayer.is_playing('jump'):
		$AnimationPlayer.play('jump')
func _on_Area2D_body_entered( body ):
	if body.name=='Player':
	#Player se vuelve invulnerable para evitar re entrar a batalla automaticamente al salir de ella
		if global.side_player.invulnerable==false:
			#global.side_player.invulnerable=true
			global.current_enemy=self
			signal_manager.transicion_a_batalla()

func _on_Timer_timeout():
	if force.y==1:
		force.y=-1
	else:
		force.y=1
		
	$Timer.start()
