extends KinematicBody2D

# Class controlling the player

# Movement Constants
const GRAVITY = 650.0
const FLOOR_NORMAL = Vector2(0, -1)
const SLOPE_SLIDE_STOP = 25.0

const WALK_FORCE = 800
const WALK_MIN = 10
const WALK_MAX = 250
const STOP_FORCE = 1300
const STOP_COEFF = 0.65

const MAX_AIR = 0.2 # Due to physics, character is always in the air. This is a tolerance


# Variables

# timer
onready var timer = $Timer

# Movement
var velocity = Vector2()


# Input
var walk_left
var walk_right
var walk_up
var walk_down
var use

#door
var door=null

# Spritework
var sees_left = false
var stopped = false
var player_sprite

# Death
var dead=false
var active=true


#mirroring
var sprite

#collision detection
var rot=0
onready var rc_down = $rc_down
var collider


func _ready():
	
	set_physics_process(true)
	set_process_input(true)
	
	player_sprite = $container/AnimationPlayer
	sprite=$container
	sprite.modulate.a = 0.6
	player_sprite.play("appear")

func _physics_process(delta):
	
	if (active):
		walk_left = Input.is_action_pressed("ui_left")
		walk_right = Input.is_action_pressed("ui_right")
		walk_up = Input.is_action_pressed("ui_up")
		walk_down = Input.is_action_pressed("ui_down")
		use= Input.is_action_just_pressed('use')
		
	# si no esta activo se setea todo a falso por que si alguno queda verdadero significa que la tecla se esta presionando
	else:
		walk_left = false
		walk_right = false
		walk_up = false
		walk_down = false
		use= false
		
	if (not dead):
		_movement(delta)
		
	#detectar colisiones
	if rc_down.is_colliding():
		sprite.modulate.a = 0.3
		player_sprite.play_backwards("disappear")
		if timer.time_left > 0.5:
			timer.set_wait_time(timer.time_left-0.1)
			timer.start()
			
	else:
		sprite.modulate.a = 0.6
		

	
func _movement(delta):
	
	# gravity
	
	var force = Vector2(0,0)
	
	# stop by default, inertia
	var stop = true

	# Sideways movement
	
	if (walk_left and not walk_right):

		if (velocity.x<=WALK_MIN and velocity.x > -WALK_MAX):
			force.x-=WALK_FORCE
			stop = false
			stopped = false
			if (!player_sprite.is_playing("move")):
				player_sprite.play("move")
		else:
			force.x = -WALK_FORCE * STOP_COEFF
	elif (walk_right and not walk_left):
	
		if (velocity.x>=-WALK_MIN and velocity.x < WALK_MAX):
			force.x+=WALK_FORCE
			stop = false
			stopped = false
			if (!player_sprite.is_playing("move")):
				player_sprite.play("move")
		else:
			force.x = WALK_FORCE * STOP_COEFF
	if (!walk_up and walk_down):
		if (velocity.y>=-WALK_MIN and velocity.y < WALK_MAX):
			force.y+=WALK_FORCE
			stop = false
			stopped = false
			if (!player_sprite.is_playing("move")):
				player_sprite.play("move")
		else:
			force.y = WALK_FORCE * STOP_COEFF
	if (!walk_down and walk_up):
		if (velocity.y<=WALK_MIN and velocity.y > -WALK_MAX):
			force.y-=WALK_FORCE
			stop = false
			stopped = false
			if (!player_sprite.is_playing("move")):
				player_sprite.play("move")
		else:
			force.y = -WALK_FORCE * STOP_COEFF

	# if the player got no movement, he'll slow down with inertia.
	if (stop and (velocity.x!=0 or velocity.y!=0)):
		stop(delta)
		
	# calculate motion
	velocity += force * delta
	
	var motion = move_and_slide(velocity,FLOOR_NORMAL,SLOPE_SLIDE_STOP)
	
	

		
	# Manage sprite mirroring
	if (velocity.x>0):
		sees_left = false
	elif (velocity.x<0):
		sees_left = true
	if (sees_left):
		sprite.scale.x=-1
	else:
		sprite.scale.x=1

	
	if Input.is_action_just_pressed('pocion'):
		use_potion()
		
	if Input.is_action_just_pressed('ghost') and active:
		destroy()
	
	if timer.is_stopped():
		destroy()
	if timer.time_left <= 0.3:
		player_sprite.play("appear")
		
	if Input.is_action_just_pressed('use') and door !=null:
		door.open()
	
#------------FUNCIONES-----------------


func stop(delta):
	
	var vsign = sign(velocity.x)
	var ysign= sign(velocity.y)
	var vy=abs(velocity.y)
	var vx = abs(velocity.x)
	vy -=STOP_FORCE*delta
	vx -= STOP_FORCE * delta
	
	if (vx<0):
		
		vx=0

	
	velocity.x=vx*vsign

	if vy<0:
		vy=0
		
	velocity.y=vy*ysign
	
	if vx==0 and vy==0:
		if (!player_sprite.is_playing("idle")):
			
			if (not stopped):
				
				stopped = true
				player_sprite.play("idle")


# func extras

func open_door():
	door.open()

func activar():
	active=true
	$Camera2D.current=true
func set_pos(r):
	global_position=r
func destroy():
	player_sprite.play_backwards("disappear")
	signal_manager.activate_player()
	self.queue_free()
# FUNCIONES DE SEÑALES

	
func _on_PlayerAnim_animation_finished( anim_name ):
	player_sprite.play("idle")

func set_camera_limits(left, top, right, bottom):
	$Camera2D.limit_left= left
	$Camera2D.limit_top = top
	$Camera2D.limit_right = right
	$Camera2D.limit_bottom = bottom
