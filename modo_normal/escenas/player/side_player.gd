extends KinematicBody2D

# Class controlling the player

# Movement Constants
const GRAVITY = 650.0
const FLOOR_NORMAL = Vector2(0, -1)
const SLOPE_SLIDE_STOP = 25.0

const WALK_FORCE = 800
const WALK_MIN = 10
const WALK_MAX = 250
const STOP_FORCE = 1300
const STOP_COEFF = 0.65

const JUMP_VEL = 300#280
const MAX_AIR = 0.2 # Due to physics, character is always in the air. This is a tolerance


# Variables

# Movement
var velocity = Vector2()
var jumping = false
var can_jump = true
var falling = false
var attacking = false # player is attacking
var can_attack = true
var air_time = 100

#Personal
var nivel=1
var experiencia=0
var monedas=0
var hp=5
var max_hp=5
var dano=1
var def=1

var pociones=0
var max_pociones=3

#door
var door=null

#skills
var jump_count=0
var max_jump=1
var root=null
var root_active=false
var avatar_active=false

#lista de skills desbloqueadas
var skills={
	"doble_jump":0,
	"raices":0,
	"avatar":0
}


# Input
var walk_left
var walk_right
var walk_up
var walk_down
var jump
var attack
var use


# Spritework
var sees_left = false
var landed = false
var stopped = false
var player_sprite

# Death
var dead = false
var active=true
var invulnerable=false
#mirroring
var sprite


#floor detection
var rot=0
onready var floor_detect = $floor_detect
var collider

#NPC
var vendedor
var vendedor_pociones
var altar
var pankratio

#INVENTARIO
var inventario=[]
var inv_arma=[]
var inv_armadura=[]
var pre_item=preload("res://modo_normal/escenas/items/item_class.gd")

var ghost=preload('res://modo_normal/escenas/player/modo_fantasma/ghost.tscn')


#EQUIPADO
var arma=null
var armadura=null

func _ready():
	monedas=0
	set_physics_process(true)
	set_process_input(true)
	
	signal_manager.connect('activate_player',self,'_on_player_activate')
	signal_manager.connect('deactivate_player',self,'_on_player_deactivate')

	
	player_sprite = $container/PlayerSprite/PlayerAnim
	sprite=$container
	
	

func _physics_process(delta):
	
	if (active):
		walk_left = Input.is_action_pressed("ui_left")
		walk_right = Input.is_action_pressed("ui_right")
		walk_up = Input.is_action_pressed("ui_up")
		walk_down = Input.is_action_pressed("ui_down")
		jump = Input.is_action_pressed("jump")
		use= Input.is_action_just_pressed('use')
		
	# si no esta activo se setea todo a falso por que si alguno queda verdadero significa que la tecla se esta presionando
	else:
		walk_left = false
		walk_right = false
		walk_up = false
		walk_down = false
		jump = false
		use= false
		
	if (not dead ):
		_movement(delta)
	
	
	if floor_detect.is_colliding() and !jump:
		collider = floor_detect.get_collider()
	
		rot = collider.rotation
		
	else:
		rot=0

		
		
func _input(event):
	if active:
		attack = event.is_action_pressed("attack")
	else:
		attack=false
		
	
func _movement(delta):
	
	# gravity
	
	var force = Vector2(0,GRAVITY)
	
	# stop by default, inertia
	var stop = true

	# Sideways movement
	# if(!is_attacking()):
	if(!player_sprite.is_playing("drink")):
		if (walk_left and not walk_right):
	
			if (velocity.x<=WALK_MIN and velocity.x > -WALK_MAX):
				force.x-=WALK_FORCE
				stop = false
				stopped = false
				if (!player_sprite.is_playing("move")  and !is_on_air() && !attacking):
					player_sprite.play("move")
			else:
				force.x = -WALK_FORCE * STOP_COEFF
		elif (walk_right and not walk_left):
		
			if (velocity.x>=-WALK_MIN and velocity.x < WALK_MAX):
				force.x+=WALK_FORCE
				stop = false
				stopped = false
				if (!player_sprite.is_playing("move") and !is_on_air() && !attacking):
					player_sprite.play("move")
			else:
				force.x = WALK_FORCE * STOP_COEFF
		
	
	# if the player got no movement, he'll slow down with inertia.
	if (stop and velocity.x!=0):
		stop(delta)
		
	# calculate motion
	velocity += force * delta
	
	var motion = move_and_slide(velocity.rotated(rot),FLOOR_NORMAL,SLOPE_SLIDE_STOP)
	
	if ( is_on_floor()):
			
			
			air_time=0
			velocity.y=0
			
			falling = false
			jump_count=0
			if (not landed):
				
				landed = true
				player_sprite.play("idle")
				
			
	air_time+=delta
	
	if (jumping and velocity.y>=0):
		
		falling = true
		jumping=false
		
	if (velocity.y > 0 and air_time > MAX_AIR):
		landed =false
		falling = true
	
	#if (velocity.y < 0 and !player_sprite.is_playing("jump") and (!is_attacking() and !is_jump_attack()) and jump_count == 1):
	#	player_sprite.play("jump")
	#	player_sprite.seek(0.3)
	if (velocity.y < 0 and !player_sprite.is_playing("jump_2") and (!is_attacking() and !is_jump_attack()) and jump_count != 1):
		player_sprite.play("jump_2")
		

	if (falling and !jumping and !player_sprite.is_playing("fall") and (!is_attacking() and !is_jump_attack())):
		
		player_sprite.play("fall")
		if (jump_count == 0 and max_jump>=1):
			jump_count+=1
	
	# Manage jumping
	if (!jump):
		
		can_jump = true
		
	elif (can_jump and jump  and jump_count < max_jump):
		if (!player_sprite.is_playing("drink")):
			landed=false
			can_jump = false
			jumping=true
			if jump_count!=1:
				player_sprite.play("jump")
			
			velocity.y = -JUMP_VEL
			jump_count+=1
	#ataque
#	if(!attack):
#
#		can_attack=true
#
#	elif(attack &&can_attack && !attacking):
#
#		can_attack=false
#		attack()
		
	
	
	# Make sure attack fixes
	if (attacking and (!is_attacking() and !is_jump_attack())):
		
		attacking=false
	
		
	# Manage sprite mirroring
	if (velocity.x>0):
		sees_left = false
	elif (velocity.x<0):
		sees_left = true
	if (sees_left):
		sprite.scale.x=-1
	else:
		sprite.scale.x=1

	# activar vendedor
	if  use and vendedor!=null:
		signal_manager.emit_signal('activate_vendor',vendedor)
	if use and vendedor_pociones!=null:
		signal_manager.emit_signal('vendedor_pociones')
	#abrir puerta
	if use and root !=null and root_active:
		root.open()
	if use and altar != null:
		altar.activar()
	if use and pankratio != null:
		pankratio.activar()
		
	
	if Input.is_action_just_pressed('pocion') and active:
		use_potion()
	
	if Input.is_action_just_pressed('inventario') and active:
		signal_manager.emit_signal('activate_inventory')
		
	if Input.is_action_just_pressed('opciones') and active:
		signal_manager.emit_signal('activate_options')
		
#	if Input.is_action_just_pressed('save'):
#		save_load.save_game()
	
	if Input.is_action_just_pressed('ghost') and active and avatar_active:
		spawn_ghost()
	
	if Input.is_action_just_pressed('use') and door !=null:
		door.open()
	
#------------FUNCIONES-----------------


func stop(delta):
	
	var vsign = sign(velocity.x)
	var vx = abs(velocity.x)
	vx -= STOP_FORCE * delta
	
	if (vx<0):
		
		vx=0
		if (!player_sprite.is_playing("idle") && !is_on_air() &&!is_attacking()):
			
			if (not stopped):
				
				stopped = true
				player_sprite.play("idle")
	
	velocity.x=vx*vsign



# combo de ataques normal
func attack():
		
		attacking = true
		
		if(is_on_air() and !player_sprite.is_playing("jump_attack")):
			player_sprite.play('jump_attack')
		
		elif (!player_sprite.is_playing("combo1")):
			player_sprite.play("combo1")
		

func slow_attack():
	attacking=false

#---HELPERS-------

#funcion para checkear si el personaje esta atacando
func is_attacking():
	if (player_sprite.is_playing("combo1")):
		return true
	else:
		return false
func is_jump_attack():
	if (player_sprite.is_playing("jump_attack")):
		return true
	else:
		return false

#checkea si esta en el aire el player
func is_on_air():
	if jumping or falling:
		return true
	
	return false

#añade una arma al inventario de armas
func add_arma(a):
	inv_arma.append(a)

#añade una armadura al inventario de armaduras
func add_armadura(a):
	
	inv_armadura.append(a)
	
func resta_monedas(n):
	monedas=monedas-n
	
func inv_array(inven):
	var arr=[]
	for i in inven:
		arr.append(i.nombre)
	return arr
func save_state():
	var save_dict={
			filename=get_filename(),
			pos={
				x=position.x,
				y=position.y
				},
			hp=hp,
			max_hp=max_hp,
			experiencia=experiencia,
			nivel=nivel,
			monedas=monedas,
			pociones=pociones,
			inventario=inventario,
			inv_arma=inv_array(inv_arma),
			inv_armadura=inv_array(inv_armadura),
			arma=null,
			armadura=null,
			skills=skills
	}
	if arma!=null:
		save_dict['arma']=arma.nombre
	if armadura !=null:
		save_dict['armadura']=armadura.nombre
	
	return save_dict
	
func load_state(dict,pos=false):
	hp=dict['hp']
	max_hp=dict['max_hp']
	experiencia=dict['experiencia']
	nivel=int(dict['nivel'])
	monedas=int(dict['monedas'])
	pociones=int(dict['pociones'])
	#dict['inv_arma']
	
	load_armas(dict['inv_arma'])
	load_skills(dict['skills'])
	
	set_skills()
	# se recorre el inventario para dejar equipada el arma que tenia
	if !inv_arma.empty() and dict['arma']!=null:
		for i in inv_arma:
			if i.nombre==dict['arma']:
				arma=i
	if pos:
		position=Vector2(dict['pos']['x'],dict['pos']['y'])


func set_skills():
	if skills['doble_jump']==1:
		max_jump=2
	if skills['raices']==1:
		root_active=true
	if skills['avatar']==1:
		avatar_active=true

func load_skills(skill_dict):
	skills['doble_jump']=skill_dict['doble_jump']
	skills['raices']=skill_dict['raices']
	skills['avatar']=skill_dict['avatar']
	

#busca las armas en la bd y las carga en el inventario utilizando un array de nombres de arma
func load_armas(array_arma):
	if !array_arma.empty():
		if SQLite.open():
	
			for a in array_arma:
				
				var query="SELECT arma.*  FROM arma where nombre='"+str(a)+"'"
				
				var res=SQLite.fetch_array(query)
				
				#fixear este se agregan 2 armas
				for arma in res:
					var item=pre_item.new(arma['nombre'],arma['descripcion'],1,arma['valor'],arma['dano'])
					add_child(item)
					inv_arma.append(item)
			
			SQLite.close()
		else:
			print('func load armas, couldnt connect to db')
	else:
		print('func load_armas empty array')
		
		
func has_item(name):
	for i in inv_arma:
		if i.nombre==name:
			return true
	for i in inv_armadura:
		if i.nombre==name:
			return true
	return false
	
func gain_exp(xp):
	
	experiencia+=xp
	var limit=100*(nivel+1)^(3/2)
	
	if experiencia>=limit:
		
		experiencia=experiencia-limit
		nivel+=1
		signal_manager.lvl_up()
		
		if nivel%2==0:
			hp=max_hp
			pass
		else:
			max_hp+=1
			hp=max_hp
			
		#def+=1

func open_roots():
	root.open()

func unlock_skill(nombre):
	if skills[nombre]!=null:
		skills[nombre]=1
		set_skills()
		return true
	return false
	
	
func use_potion():
	if player_sprite.is_playing("idle"):
		if pociones>0 and hp<max_hp:
			pociones-=1
			player_sprite.play("drink")
			hp+=3
			if hp>max_hp:
				hp=max_hp
			signal_manager.update_hp(hp)
			signal_manager.pociones_change()
	
func spawn_ghost():
	var g_instance=ghost.instance()
	var g_cam=get_node("Camera2D")
	g_instance.activar()
	g_instance.set_pos(global_position)
	g_instance.set_camera_limits(g_cam.limit_left, g_cam.limit_top, g_cam.limit_right, g_cam.limit_bottom)
	get_parent().add_child(g_instance)
	signal_manager.deactivate_player()
# FUNCIONES DE SEÑALES

	
func _on_PlayerAnim_animation_finished( anim_name ):
	player_sprite.play("idle")

	
#activa al jugador al detectar la señal
func _on_player_activate():
	active=true
	
#desactiva al jugador al detectar la señal
func _on_player_deactivate():
	active=false
	

func _on_combo_hitbox_area_entered( area ):
	
	var enemigo=area.get_parent()
	if enemigo.is_in_group('enemigo'):
		if global.side_player.invulnerable==false:
			global.side_player.invulnerable=true
			global.current_enemy=enemigo
			signal_manager.transicion_a_batalla()
	

func open_door():
	door.open()
