extends StaticBody2D

# class member variables go here, for example:
# var a = 2
var door_sprite

func _ready():
	door_sprite= $AnimatedSprite/AnimationPlayer
	# Called when the node is added to the scene for the first time.
	# Initialization here

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
func open():
	door_sprite.play("open_door")
	$CollisionShape2D.disabled=true


func _on_boton_body_entered(body):
	print(body.name)
	if body.name=='ghost':
		body.door=self
	elif body.name=='Player':
		body.door=self

func _on_boton_body_exited(body):
	if body.name=='ghost':
		body.door=null
	elif body.name=='Player':
		body.door=null
