extends Node2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
export var value = 1


func _ready():

	pass


func _collect_coin( body ):
	
	if body.name=='Player':
		global.side_player.monedas += value
		signal_manager.emit_signal("money_change")
		queue_free()
	

