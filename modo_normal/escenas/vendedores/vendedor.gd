extends Node2D

var pre_item=preload("res://modo_normal/escenas/items/item_class.gd")
var inventario=[]
var nombre
func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	load_inventory()


func load_inventory():
	if SQLite.open():
		var query="SELECT arma.*  FROM arma INNER JOIN vendedor_arma ON arma.id=vendedor_arma.arma_id LEFT JOIN vendedor ON vendedor_id=vendedor.id where vendedor_arma.vendedor_id='1'"
		var res=SQLite.fetch_array(query)
		
		for arma in res:
			var item=pre_item.new(arma['nombre'],arma['descripcion'],1,arma['valor'],arma['dano'])
			add_child(item)
			inventario.append(item)
		
		SQLite.close()



#estas 2 funciones setean la variable vendedor en el player que sirve para activar la compra y cargar los items
#ya que el player se puede acceder globalmente a sus variables
func _on_Area2D_body_entered( body ):
	if body.name=='Player':
		
		body.vendedor=self
	


func _on_Area2D_body_exited( body ):
	if body.name=='Player':
		body.vendedor=null
	