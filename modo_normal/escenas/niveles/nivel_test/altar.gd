extends Node2D


var activado=false

export (String) var skill
export(String, FILE) var file_path

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass
#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass

#activa el altar
func activar():
	if skill !=null and global.side_player.skills[skill]==1:
		activado=true
	if activado==false:
		if skill != "":
			$AnimationPlayer.play('diosa_activa')
			global.side_player.unlock_skill(skill)
			signal_manager.activate_dialogue(file_path,self)
			
		if skill =="":
			activado=true
	
	if activado==true:
		show_gui()
	signal_manager.save_game()
	save_load.save_game()

#al terminar el dialogo se activa el jugador

func end_dialogue():
	if activado==false:
		activado=true
		#signal_manager.activate_player()
		$AnimationPlayer.play('diosa_desactiva')
		show_gui()


func show_gui():
	
	signal_manager.altar_gui()



#cuando el area detecta al player dentro o fuera se actualiza la variable altar del player

func _on_Area2D_body_entered( body ):
	if body.name=='Player':
		global.current_place="start"
		body.altar=self

func _on_Area2D_body_exited( body ):
	if body.name=='Player':
		body.altar=null