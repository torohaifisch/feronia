extends Node2D
export (int )var bg

func _ready():
	#esto es para saber en que escena estoy por ahora se hace desde aca pero deberia manejarse desde 
	#el global cuando se entre a sidescroll scene desde el overworld
	
	global.set_sidescroll_scene()
	print(global.sidescroll_scene.name)
	global.set_sideplayer()
		
	#para cargar el dinero actual al iniciar el nivel
	signal_manager.emit_signal("money_change")
	
	#manejo de posiciones dependiendo del nivel
	if global.current_place == "level0 from level1":
		get_node("Player").set_position(Vector2(3190,385))
	elif global.current_place == "level0 from level1_2":
		get_node("Player").set_position(get_node("toLevel1_2from0").get_position()+Vector2(50,135))
	elif global.current_place == "level1 from level1_3":
		get_node("Player").set_position(get_node("Level1to1_3").get_position()+Vector2(-50,135)) 
	elif global.current_place == "level2 from level2_1":
		get_node("Player").set_position(get_node("Level2to2_1").get_position()+Vector2(-50,140))
	elif global.current_place == "level1 from level2_1":
		get_node("Player").set_position(get_node("colisiones_trans/transpasable33").get_position()+Vector2(0,-50))
	elif global.current_place == "level2_1 from level1":
		get_node("Player").set_position(get_node("colisiones_trans/StaticBody2D5").get_position()+Vector2(0,-70))
	elif global.current_place == "level1_3 from level3":
		get_node("Player").set_position(get_node("toLevel3from1_3").get_position()+Vector2(50, 50))
	elif global.current_place == "level1_3 from level4":
		get_node("Player").set_position(get_node("toLevel4from1_3").get_position()+Vector2(-50, 50))
	


#al colisionar con el piso gameover mandar señal
func _on_gameover_body_entered( body ):
	if body.name=='Player':
		global.current_place="start"
		signal_manager.game_over_b()
		

#paso entre niveles
func _on_end_body_entered( body ):
	if body.name=='Player':
		global.current_place="level1 from level0"
		global.start_mode(self,1)


func _on_toLevel2from1_2_body_entered( body ):
	if body.name=='Player':
		global.current_place="level2 from level1_2"
		global.start_mode(self,4)


func _on_toLevel1_2from0_body_entered(body):
	if body.name=='Player':
		global.current_place="level1_2 from level0"
		global.start_mode(self,3)


func _on_toLevel0from1_2_body_entered(body):
	if body.name=='Player':
		global.current_place="level0 from level1_2"
		global.start_mode(self,0)


func _on_toLevel0from1_body_entered(body):
	if body.name=='Player':
		global.current_place="level0 from level1"
		global.start_mode(self,0)

func _on_Level2to2_1_body_entered(body):
	if body.name=='Player':
		global.current_place="level2_1 from level2"
		global.start_mode(self,5)

func _on_Level1to2_1_body_entered(body):
	if body.name=='Player':
		global.current_place="level2_1 from level1"
		global.start_mode(self,5)

func _on_Level2_1to1_body_entered(body):
	if body.name=='Player':
		global.current_place="level1 from level2_1"
		global.start_mode(self,1)


func _on_Level1to1_3_body_entered(body):
	if body.name=='Player':
		global.current_place="level1_3 from level1"
		global.start_mode(self,6)

func _on_toLevel1from1_3_body_entered(body):
	if body.name=='Player':
		global.current_place="level1 from level1_3"
		global.start_mode(self,1)

func _on_Level2_1to2_body_entered(body):
	if body.name=='Player':
		global.current_place="level2 from level2_1"
		global.start_mode(self,4)

func _on_toLevel1_3from3_body_entered(body):
	if body.name=='Player':
		global.current_place="level1_3 from level3"
		global.start_mode(self,6)

func _on_toLevel3from1_3_body_entered(body):
	if body.name=='Player':
		global.current_place="level3 from level1_3"
		global.start_mode(self,7)

func _on_toLevel4from1_3_body_entered(body):
	if body.name=='Player':
		global.current_place="level4 from level1_3"
		global.start_mode(self,8)


func _on_toLevel1_3from4_body_entered(body):
	if body.name=='Player':
		global.current_place="level1_3 from level4"
		global.start_mode(self,6)

#cambio de background en los niveles
func _on_change_bg_body_entered(body):
	if body.name=='Player':
		if bg == 1:
			bg = 2

func _on_change_bg_body_exited(body):
	if body.name=='Player':
		if bg == 2:
			bg = 1

func _on_bg_change_body_entered(body):
	if body.name=='Player':
		if bg == 1:
			bg = 3

func _on_bg_change_body_exited(body):
	if body.name=='Player':
		if bg == 3:
			bg = 1
			

