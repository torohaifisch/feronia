extends Panel

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
#export(String, FILE) var file_path
var active=false
var can_continue=false
var current_line=0
var animplayer
var text_data
var calling_node

var pos1=Vector2(-34,12)
var pos2=Vector2(653,12)
var personaje1
var personaje2

var sprites={
			"Feronia":"res://modo_normal/dialogos/sprites/feronia/feronia_spriteframes.tres",
			"Pankratio":"res://modo_normal/dialogos/sprites/pankratio/pankratio_spriteframes.tres",
			"Isma":"res://modo_normal/dialogos/sprites/isma/isma_spriteframes.tres",
			"Diosa":"res://modo_normal/dialogos/sprites/diosa/diosa_spriteframes.tres"
			
			}
func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	signal_manager.connect('activate_dialogue',self,'activate')
	animplayer=$AnimationPlayer
	#activate()
	set_physics_process(true)

func _physics_process(delta):
	if Input.is_action_just_pressed('attack'):
		next_line()
func activate(file_path,node):
	signal_manager.deactivate_player()
	active=true
	visible=true
	current_line=0
	text_data=null
	calling_node=node
	load_file(file_path)
	load_text()
	

func deactivate():
	active=false
	visible=false
	end_func()
	
	
func next_line():
	if can_continue==true:
		can_continue=false
		current_line+=1
		if current_line<text_data['dialogo'].size():
			load_text()
		else:
			deactivate()
			
func load_text():
	$MarginContainer/text.text=text_data['dialogo'][current_line]['linea']
	if text_data['dialogo'][current_line]['personaje']== 1:
		$p2.self_modulate=Color("#787878")
		$p1.self_modulate=Color("#ffffff")
	else:
		$p2.self_modulate=Color("#ffffff")
		$p1.self_modulate=Color("#787878")
	animplayer.play("show_text")
	


func can_continue():
	can_continue=true

func load_file(file_path):
	var dialogue=File.new()
	if !dialogue.file_exists(file_path):
		print("no existe archivo")
		return
	dialogue.open(file_path,File.READ)
	text_data=parse_json(dialogue.get_as_text())
	print(text_data['dialogo'][current_line])
	load_sprites(text_data)
	
func load_sprites(text_data):
	if text_data['personajes']!=null:
		
		if text_data['personajes'][0] !=null:
			
			var frames=load(sprites[text_data.personajes[0]])
			$p1.frames=frames
		
		if text_data.personajes[1] !=null:
			
			var frames=load(sprites[text_data.personajes[1]])
			$p2.frames=frames

func end_func():
	if calling_node != null:
		calling_node.end_dialogue()
		calling_node=null
	else:
		print('calling node doesnt exist')
	