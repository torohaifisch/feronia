extends StaticBody2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
func open():
	
	$CollisionShape2D.disabled=true

func _on_Area2D_body_entered( body ):
	if body.name=='Player':
		body.root=self


func _on_Area2D_body_exited( body ):
	if body.name=='Player':
		body.root=null