extends AnimatedSprite

var activado=false

export(String, FILE) var file_path

func _ready():
	$".".modulate.a = 0.6
	# Called every time the node is added to the scene.
	# Initialization here
	pass
#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass

func activar():
	if activado==false:

		signal_manager.activate_dialogue(file_path,self)

func end_dialogue():
	if activado==false:
		activado=true
		signal_manager.activate_player()


func _on_Area2D_body_entered( body ):
	if body.name=='Player':
		body.pankratio=self

func _on_Area2D_body_exited( body ):
	if body.name=='Player':
		body.pankratio=null